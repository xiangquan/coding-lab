#include <algorithm>
#include <map>
#include <set>
#include <tuple>
#include <vector>
using namespace std;

class Solution {
 public:
  typedef tuple<int, int, int> Building;  // x, is_left, hight

  vector<vector<int>> getSkyline(vector<vector<int>>& buildings) {
    map<tuple<int, int, int>, Building> all_buildings;
    for (int i = 0; i < buildings.size(); ++i) {
      const auto& building = buildings[i];
      const Building b = std::make_tuple(building[0], building[1], building[2]);
      // Add i to key to make it unique.
      all_buildings.emplace(std::make_tuple(building[0], 0, i), b);
      all_buildings.emplace(std::make_tuple(building[1], 1, i), b);
    }

    set<Building> active_buildings;
    vector<vector<int>> result;
    for (const auto& building : all_buildings) {
      int left, right, height;
      std::tie(left, right, height) = building.second;

      const int x = std::get<0>(building.first);
      const int current_height = result.empty() ? 0 : result.back().at(1);
      if (x == left) {
        // Add new active building.
        active_buildings.insert(building.second);
        if (height > current_height) {
          appendVertex(x, height, &result);
        }
      } else {
        active_buildings.erase(building.second);
        // Find current highest active building.
        height = 0;
        for (const Building& active_building : active_buildings) {
          height = std::max(height, std::get<2>(active_building));
        }
        if (height < current_height) {
          appendVertex(x, height, &result);
        }
      }
    }
    return result;
  }

 private:
  static void appendVertex(const int x, const int h, vector<vector<int>>* result) {
    if (!result->empty() && result->back().at(0) == x) {
      result->back().at(1) = h;
    } else {
      result->push_back({x, h});
    }
  }
};
