#include <vector>
using namespace std;

class Solution {
 public:
  void moveZeroes(vector<int>& nums) {
    int w = 0;
    for (int i = 0; i < nums.size(); ++i) {
      if (nums[i]) {
        nums[w++] = nums[i];
      }
    }
    for (int i = nums.size() - 1; i >= w; --i) {
      nums[i] = 0;
    }
  }
};
