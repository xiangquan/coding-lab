#include <queue>
#include <vector>
using namespace std;

struct TreeNode {
  int val;
  TreeNode* left;
  TreeNode* right;
  TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
};

class Solution {
 public:
  vector<int> rightSideView(TreeNode* root) {
    vector<int> result;
    if (root == nullptr) {
      return result;
    }
    queue<TreeNode*> level;
    level.push(root);
    level.push(nullptr);
    while (!level.empty()) {
      TreeNode* front = level.front();
      level.pop();
      if (front == nullptr) {
        continue;
      }

      if (front->left) {
        level.push(front->left);
      }
      if (front->right) {
        level.push(front->right);
      }
      if (level.front() == nullptr) {
        level.pop();
        result.push_back(front->val);
        level.push(nullptr);
      }
    }
    return result;
  }
};
