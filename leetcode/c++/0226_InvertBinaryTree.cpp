#include <queue>
using namespace std;

// Definition for a binary tree node.
struct TreeNode {
  int val;
  TreeNode* left;
  TreeNode* right;
  TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
};

class Solution {
 public:
  TreeNode* invertTree(TreeNode* root) {
    queue<TreeNode*> todo;
    if (root) {
      todo.push(root);
    }

    while (!todo.empty()) {
      auto* node = todo.front();
      todo.pop();

      auto* left = node->left;
      node->left = node->right;
      node->right = left;

      if (node->left) {
        todo.push(node->left);
      }
      if (node->right) {
        todo.push(node->right);
      }
    }
    return root;
  }
};
