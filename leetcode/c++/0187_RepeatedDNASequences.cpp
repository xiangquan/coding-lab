#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>
using namespace std;

const size_t kPieceLen = 10;

class Solution {
 public:
  vector<string> findRepeatedDnaSequences(string s) {
    unordered_set<string> result;
    PrefixTreeNode prefixTree;
    for (size_t i = 0; i + kPieceLen <= s.length(); ++i) {
      if (prefixTree.insert(s, i, 0)) {
        result.insert(s.substr(i, kPieceLen));
      }
    }
    return {result.begin(), result.end()};
  }

 private:
  struct PrefixTreeNode {
    unordered_map<char, PrefixTreeNode*> children;
    int count = 0;

    bool insert(const string& s, const size_t index, const size_t len) {
      if (len == kPieceLen) {
        return ++count >= 2;
      }
      const char key = s[index + len];
      if (children.find(key) == children.end()) {
        children.emplace(key, new PrefixTreeNode());
      }
      return children[key]->insert(s, index, len + 1);
    }
  };
};
