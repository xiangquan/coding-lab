#include <vector>
using namespace std;

class Solution {
 public:
  int maximumGap(vector<int>& nums) { return maximumGap(nums, 1); }

 private:
  int maximumGap(vector<int>& nums, int common_bits) {
    if (common_bits == 32 || nums.size() < 2) {
      return 0;
    }
    vector<int> start_with_0, start_with_1;
    int largest_0 = -1, smallest_1 = -1;
    const int mask = 1 << (32 - common_bits - 1);
    for (const int num : nums) {
      if (num & mask) {
        start_with_1.push_back(num);
        smallest_1 = smallest_1 == -1 ? num : std::min(smallest_1, num);
      } else {
        start_with_0.push_back(num);
        largest_0 = largest_0 == -1 ? num : std::max(largest_0, num);
      }
    }
    int result = std::max(maximumGap(start_with_0, common_bits + 1),
                          maximumGap(start_with_1, common_bits + 1));
    if (largest_0 != -1 && smallest_1 != -1) {
      result = std::max(result, smallest_1 - largest_0);
    }
    return result;
  }
};
