#include <vector>
using namespace std;

class Solution {
 public:
  void rotate(vector<int>& nums, int k) {
    k %= nums.size();
    rotate(nums, 0, nums.size() - 1);
    rotate(nums, 0, k - 1);
    rotate(nums, k, nums.size() - 1);
  }

  void rotate(vector<int>& nums, int l, int r) {
    while (l < r) {
      int tmp = nums[l];
      nums[l] = nums[r];
      nums[r] = tmp;
      ++l;
      --r;
    }
  }
};
