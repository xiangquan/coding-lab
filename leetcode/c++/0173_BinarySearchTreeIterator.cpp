#include <stack>
using namespace std;

struct TreeNode {
  int val;
  TreeNode* left;
  TreeNode* right;
  TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
};

class BSTIterator {
 public:
  BSTIterator(TreeNode* root) { push(root); }

  /** @return the next smallest number */
  int next() {
    TreeNode* node = nodes_.top();
    nodes_.pop();
    push(node->right);
    return node->val;
  }

  /** @return whether we have a next smallest number */
  bool hasNext() { return !nodes_.empty(); }

 private:
  void push(TreeNode* node) {
    while (node) {
      nodes_.push(node);
      node = node->left;
    }
  }

  stack<TreeNode*> nodes_;
};

/**
 * Your BSTIterator object will be instantiated and called as such:
 * BSTIterator* obj = new BSTIterator(root);
 * int param_1 = obj->next();
 * bool param_2 = obj->hasNext();
 */
