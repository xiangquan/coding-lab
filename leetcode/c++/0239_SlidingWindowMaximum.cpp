#include <set>
#include <vector>
using namespace std;

class Solution {
 public:
  vector<int> maxSlidingWindow(vector<int>& nums, int k) {
    vector<int> result;
    if (nums.empty() || k == 0) {
      return result;
    }

    multiset<int> window;
    for (int i = 0; i < k; ++i) {
      window.insert(nums[i]);
    }

    result.push_back(*window.rbegin());
    for (int i = k; i < nums.size(); ++i) {
      const int to_remove = nums[i - k];
      window.erase(window.lower_bound(to_remove));
      window.insert(nums[i]);
      if (nums[i] >= result.back()) {
        result.push_back(nums[i]);
      } else if (to_remove != result.back()) {
        // The one removed is not the largest.
        result.push_back(result.back());
      } else {
        result.push_back(*window.rbegin());
      }
    }
    return result;
  }
};
