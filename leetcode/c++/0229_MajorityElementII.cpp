#include <vector>
using namespace std;

class Solution {
 public:
  vector<int> majorityElement(vector<int>& nums) {
    int candidate1, candidate2;
    int count1 = 0, count2 = 0;
    for (const int num : nums) {
      // Hit candidate.
      if (count1 > 0 && candidate1 == num) {
        ++count1;
        continue;
      }
      if (count2 > 0 && candidate2 == num) {
        ++count2;
        continue;
      }

      // Fill slot, or demote current candidates.
      if (count1 == 0) {
        candidate1 = num;
        ++count1;
      } else if (count2 == 0) {
        candidate2 = num;
        ++count2;
      } else {
        --count1;
        --count2;
      }
    }

    count1 = count2 = 0;
    for (const int num : nums) {
      if (candidate1 == num) {
        ++count1;
      } else if (candidate2 == num) {
        ++count2;
      }
    }

    vector<int> result;
    if (count1 > nums.size() / 3) {
      result.push_back(candidate1);
    }
    if (count2 > nums.size() / 3) {
      result.push_back(candidate2);
    }
    return result;
  }
};
