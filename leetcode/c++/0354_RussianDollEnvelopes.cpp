#include <algorithm>
#include <vector>
using namespace std;

class Solution {
 public:
  int maxEnvelopes(vector<vector<int>>& envelopes) {
    if (envelopes.empty()) {
      return 0;
    }
    std::sort(envelopes.begin(), envelopes.end());

    vector<int> count(envelopes.size(), 1);
    int cur_max = 1;
    for (int i = 1; i < envelopes.size(); ++i) {
      int my_max = 1;
      for (int j = i - 1; j >= 0; --j) {
        if (envelopes[i][0] <= envelopes[j][0] || envelopes[i][1] <= envelopes[j][1]) {
          continue;
        }

        my_max = max(my_max, count[j] + 1);
        if (my_max > cur_max) {
          cur_max = my_max;
          break;
        }
      }
      count[i] = my_max;
    }
    return cur_max;
  }
};
