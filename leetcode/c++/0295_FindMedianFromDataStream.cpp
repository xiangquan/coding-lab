#include <algorithm>
#include <functional>
#include <vector>
using namespace std;

class MedianFinder {
 public:
  MedianFinder() {}

  void addNum(int num) {
    if (!large_nums.empty() && num >= large_nums[0]) {
      large_nums.push_back(num);
      std::push_heap(large_nums.begin(), large_nums.end(), std::greater<int>());
      num = large_nums[0];
      std::pop_heap(large_nums.begin(), large_nums.end(), std::greater<int>());
      large_nums.pop_back();
    }

    small_nums.push_back(num);
    std::push_heap(small_nums.begin(), small_nums.end());

    // Rebalance.
    while (small_nums.size() - 1 > large_nums.size()) {
      large_nums.push_back(small_nums[0]);
      std::push_heap(large_nums.begin(), large_nums.end(), std::greater<int>());

      std::pop_heap(small_nums.begin(), small_nums.end());
      small_nums.pop_back();
    }
  }

  double findMedian() {
    if (small_nums.empty()) {
      return 0;
    }
    if (small_nums.size() == large_nums.size()) {
      return (small_nums[0] + large_nums[0]) / 2.0;
    }
    return small_nums[0];
  }

 private:
  vector<int> small_nums;
  vector<int> large_nums;
};
