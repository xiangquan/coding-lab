#include <string>
using namespace std;

class Solution {
 public:
  string reverseVowels(string s) {
    int l = 0, r = s.length() - 1;

    while (l < r) {
      while (l < r && !IsVowel(s[l])) {
        ++l;
      }
      while (l < r && !IsVowel(s[r])) {
        --r;
      }
      if (l < r) {
        char tmp = s[l];
        s[l] = s[r];
        s[r] = tmp;
      }
      ++l, --r;
    }
    return s;
  }

  bool IsVowel(char ch) {
    return ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u' || ch == 'A' ||
           ch == 'E' || ch == 'I' || ch == 'O' || ch == 'U';
  }
};
