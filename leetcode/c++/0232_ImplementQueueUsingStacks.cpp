#include <stack>
using namespace std;

class Queue {
 public:
  // Push element x to the back of queue.
  void push(int x) { to_push.push(x); }

  // Removes the element from in front of queue.
  void pop(void) {
    if (to_pop.empty()) {
      while (!to_push.empty()) {
        to_pop.push(to_push.top());
        to_push.pop();
      }
    }
    to_pop.pop();
  }

  // Get the front element.
  int peek(void) {
    if (to_pop.empty()) {
      while (!to_push.empty()) {
        to_pop.push(to_push.top());
        to_push.pop();
      }
    }
    return to_pop.top();
  }

  // Return whether the queue is empty.
  bool empty(void) { return to_pop.empty() && to_push.empty(); }

 private:
  stack<int> to_pop;
  stack<int> to_push;
};
