#include <vector>
using namespace std;

class Solution {
 public:
  int majorityElement(vector<int>& nums) {
    if (nums.size() <= 2) {
      return nums[0];
    }

    vector<int> ret;
    for (int i = 0; i + 1 < nums.size(); i += 2) {
      if (nums[i] == nums[i + 1]) {
        ret.push_back(nums[i]);
      }
    }
    if (nums.size() % 2) {
      ret.push_back(nums.back());
    }
    return majorityElement(ret);
  }
};
