#include <stack>
#include <unordered_set>
#include <vector>
using namespace std;

// Definition for a binary tree node.
struct TreeNode {
  int val;
  TreeNode* left;
  TreeNode* right;
  TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
};

class Solution {
 public:
  vector<int> postorderTraversal(TreeNode* root) {
    vector<int> result;
    if (!root) {
      return result;
    }

    unordered_set<TreeNode*> visited;
    stack<TreeNode*> todo;
    todo.push(root);

    while (!todo.empty()) {
      TreeNode* node = todo.top();
      if (visited.find(node) == visited.end()) {
        if (node->right) {
          todo.push(node->right);
        }
        if (node->left) {
          todo.push(node->left);
        }
        visited.insert(node);
      } else {
        result.push_back(node->val);
        todo.pop();
      }
    }
    return result;
  }
};
