class Solution {
 public:
  int computeArea(int A, int B, int C, int D, int E, int F, int G, int H) {
    int sum = (C - A) * (D - B) + (G - E) * (H - F);

    if (A < E) {
      A = E;
    } else {
      E = A;
    }
    if (B < F) {
      B = F;
    } else {
      F = B;
    }
    if (C < G) {
      G = C;
    } else {
      C = G;
    }
    if (D < H) {
      H = D;
    } else {
      D = H;
    }

    if (A >= C || B >= D) {
      return sum;
    }
    return sum - (C - A) * (D - B);
  }
};
