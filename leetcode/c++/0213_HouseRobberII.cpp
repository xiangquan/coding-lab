#include <algorithm>
#include <vector>
using namespace std;

class Solution {
 public:
  int rob(vector<int>& nums) {
    if (nums.empty()) {
      return 0;
    }
    int with_last, without_last;
    rob_no_circle(nums, 2, &with_last, &without_last);
    const int with_first = nums[0] + without_last;
    const int without_first = rob_no_circle(nums, 1, &with_last, &without_last);
    return std::max(with_first, without_first);
  }

 private:
  int rob_no_circle(const vector<int>& nums, const int start, int* with_last, int* without_last) {
    vector<int> with_current = nums;
    vector<int> without_current(nums.size(), 0);
    for (int i = start + 1; i < nums.size(); ++i) {
      with_current[i] += without_current[i - 1];
      without_current[i] = std::max(with_current[i - 1], without_current[i - 1]);
    }
    *with_last = with_current.back();
    *without_last = without_current.back();
    return std::max(*with_last, *without_last);
  }
};
