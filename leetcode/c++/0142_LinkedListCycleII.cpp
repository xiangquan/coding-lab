// Definition for singly-linked list.
struct ListNode {
  int val;
  ListNode *next;
  ListNode(int x) : val(x), next(nullptr) {}
};

class Solution {
 public:
  ListNode *detectCycle(ListNode *head) {
    ListNode *h1 = head;
    ListNode *h2 = head;

    if (!head || !head->next) {
      return nullptr;
    }

    h1 = h1->next;
    h2 = h2->next->next;
    while (h1 != h2) {
      if (!h2 || !h2->next) {
        return nullptr;
      }
      h1 = h1->next;
      h2 = h2->next->next;
    }

    h1 = head;
    while (h1 != h2) {
      h1 = h1->next;
      h2 = h2->next;
    }
    return h1;
  }
};
