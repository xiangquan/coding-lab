#include <string>
#include <unordered_map>
#include <unordered_set>
using namespace std;

class Solution {
 public:
  bool isIsomorphic(string s, string t) {
    if (s.length() != t.length()) {
      return false;
    }

    unordered_map<char, char> s2t;
    unordered_set<char> used_t;
    for (int i = 0; i < s.length(); ++i) {
      if (s2t.find(s[i]) == s2t.end()) {
        if (used_t.find(t[i]) != used_t.end()) {
          return false;
        }
        s2t[s[i]] = t[i];
        used_t.insert(t[i]);
      } else if (s2t[s[i]] != t[i]) {
        return false;
      }
    }
    return true;
  }
};
