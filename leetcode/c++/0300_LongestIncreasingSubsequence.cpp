#include <algorithm>
#include <vector>
using namespace std;

class Solution {
 public:
  int lengthOfLIS(vector<int>& nums) {
    if (nums.empty()) {
      return 0;
    }
    vector<int> maxLen(nums.size(), 1);
    for (int i = 1; i < nums.size(); ++i) {
      for (int j = 0; j < i; ++j) {
        if (nums[i] > nums[j]) {
          maxLen[i] = std::max(maxLen[i], maxLen[j] + 1);
        }
      }
    }
    return *std::max_element(maxLen.begin(), maxLen.end());
  }
};
