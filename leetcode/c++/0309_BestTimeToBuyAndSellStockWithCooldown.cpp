#include <vector>

using std::vector;

class Solution {
 public:
  int maxProfit(vector<int>& prices) {
    if (prices.empty()) {
      return 0;
    }
    vector<int> profit_sale_at_i(prices.size(), 0);
    vector<int> profit_no_sale_at_i(prices.size(), 0);
    for (int i = 1; i < prices.size(); ++i) {
      profit_no_sale_at_i[i] = std::max(profit_sale_at_i[i - 1], profit_no_sale_at_i[i - 1]);

      // Buy at j and sell at i.
      int max_profit = 0;
      for (int j = 0; j < i; ++j) {
        if (prices[j] < prices[i]) {
          int profit = prices[i] - prices[j];
          if (j > 0) {
            profit += profit_no_sale_at_i[j - 1];
          }
          max_profit = std::max(max_profit, profit);
        }
      }
      profit_sale_at_i[i] = max_profit;
    }
    return std::max(profit_sale_at_i.back(), profit_no_sale_at_i.back());
  }
};
