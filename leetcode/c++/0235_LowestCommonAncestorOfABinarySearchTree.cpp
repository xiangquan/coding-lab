// Definition for a binary tree node.
struct TreeNode {
  int val;
  TreeNode* left;
  TreeNode* right;
  TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
};

class Solution {
 public:
  TreeNode* lowestCommonAncestor(TreeNode* root, TreeNode* p, TreeNode* q) {
    while (root) {
      if (root == p || root == q) {
        return root;
      }

      TreeNode* next_p = p->val < root->val ? root->left : root->right;
      TreeNode* next_q = q->val < root->val ? root->left : root->right;
      if (next_p != next_q) {
        return root;
      }
      root = next_p;
    }
    return nullptr;
  }
};
