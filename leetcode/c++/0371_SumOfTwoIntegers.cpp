class Solution {
 public:
  int getSum(int a, int b) {
    int addon = (a & b) << 1;
    int sum = a ^ b;
    if (addon == 0) {
      return sum;
    }
    return getSum(addon, sum);
  }
};
