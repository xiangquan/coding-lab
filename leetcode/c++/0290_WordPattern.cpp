#include <string>
#include <unordered_map>
#include <unordered_set>
using namespace std;

class Solution {
 public:
  bool wordPattern(string pattern, string str) {
    unordered_map<char, string> dict;
    unordered_set<string> used_part;
    int pat_i = 0, str_i = 0;
    while (pat_i < pattern.length() && str_i < str.length()) {
      int next_space = str_i;
      while (next_space < str.length() && str[next_space] != ' ') {
        ++next_space;
      }
      const string part = str.substr(str_i, next_space - str_i);
      if (dict.find(pattern[pat_i]) == dict.end()) {
        if (used_part.find(part) != used_part.end()) {
          return false;
        }
        used_part.insert(part);
        dict[pattern[pat_i]] = part;
      } else if (dict[pattern[pat_i]] != part) {
        return false;
      }

      str_i = next_space + 1;
      ++pat_i;
    }
    return pat_i >= pattern.length() && str_i >= str.length();
  }
};
