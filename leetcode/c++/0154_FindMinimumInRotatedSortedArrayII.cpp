#include <vector>
using namespace std;

class Solution {
 public:
  int findMin(vector<int>& nums) { return findMin(nums, 0, nums.size() - 1); }

 private:
  int findMin(vector<int>& nums, int start, int end) {
    if (nums[start] < nums[end] || start == end) {
      return nums[start];
    }
    const int mid = (start + end) / 2;
    return std::min(findMin(nums, start, mid), findMin(nums, mid + 1, end));
  }
};
