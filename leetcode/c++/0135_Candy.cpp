#include <vector>
using namespace std;

class Solution {
 public:
  int candy(vector<int>& ratings) {
    if (ratings.empty()) {
      return 0;
    }

    vector<int> result(ratings.size(), 0);
    for (int i = 0; i < ratings.size(); ++i) {
      const bool lt_right = (i == ratings.size() - 1) || ratings[i] <= ratings[i + 1];
      if (!lt_right) {
        continue;
      }

      const bool lt_left = (i == 0) || ratings[i] <= ratings[i - 1];
      if (lt_left) {
        result[i] = 1;
        for (int j = i - 1; j >= 0 && ratings[j] > ratings[j + 1]; --j) {
          result[j] = result[j + 1] + 1;
          if (j > 0 && ratings[j] > ratings[j - 1]) {
            result[j] = max(result[j], result[j - 1] + 1);
          }
        }
      } else {
        result[i] = result[i - 1] + 1;
      }
    }

    int total = 0;
    for (int i = 0; i < ratings.size(); ++i) {
      total += result[i];
    }
    return total;
  }
};
