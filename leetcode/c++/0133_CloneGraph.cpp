#include <unordered_map>
#include <vector>
using namespace std;

// Definition for undirected graph.
struct UndirectedGraphNode {
  int label;
  vector<UndirectedGraphNode*> neighbors;
  UndirectedGraphNode(int x) : label(x){};
};

class Solution {
 public:
  UndirectedGraphNode* cloneGraph(UndirectedGraphNode* node) {
    unordered_map<UndirectedGraphNode*, UndirectedGraphNode*> cloned;
    return cloneGraph(node, cloned);
  }
  UndirectedGraphNode* cloneGraph(
      UndirectedGraphNode* node,
      unordered_map<UndirectedGraphNode*, UndirectedGraphNode*>& cloned) {
    if (!node) {
      return nullptr;
    }
    if (cloned.find(node) != cloned.end()) {
      return cloned[node];
    }
    UndirectedGraphNode* n = new UndirectedGraphNode(node->label);
    cloned.emplace(node, n);
    for (auto* child : node->neighbors) {
      n->neighbors.push_back(cloneGraph(child, cloned));
    }

    return n;
  }
};
