class Solution {
 public:
  int findPeakElement(vector<int>& nums) {
    if (nums.empty()) {
      return -1;
    }
    return findPeakElement(nums, 0, nums.size());
  }

 private:
  // Find peak element in nums[left, right), return its index, or -1 if none.
  int findPeakElement(const vector<int>& nums, int left, int right) {
    // 1 element.
    if (left + 1 == right) {
      return isPeak(nums, left) ? left : -1;
    }
    // 2 elements.
    if (left + 2 == right) {
      if (isPeak(nums, left)) {
        return left;
      }
      return isPeak(nums, left + 1) ? left + 1 : -1;
    }
    // 3+ elements: Check right section directly if there must be a peak element, otherwise check
    // left first.
    const int mid = (left + right) / 2;
    if (!mustHavePeak(nums, mid, right)) {
      const int result = findPeakElement(nums, left, mid);
      if (result >= 0) {
        return result;
      }
    }
    return findPeakElement(nums, mid, right);
  }

  bool isPeak(const vector<int>& nums, const int i) {
    return (i == 0 || nums[i] > nums[i - 1]) && (i + 1 == nums.size() || nums[i] > nums[i + 1]);
  }

  // Quick check if there must be peak element in nums[left, right).
  bool mustHavePeak(const vector<int>& nums, int left, int right) {
    const int mid = (left + right) / 2;
    return nums[mid] > nums[left] && nums[mid] > nums[right - 1];
  }
};
