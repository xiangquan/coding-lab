#include <algorithm>
#include <vector>
using namespace std;

class Solution {
 public:
  int hIndex(vector<int>& citations) { return hIndex(citations, 0, citations.size(), 0); }

 private:
  int hIndex(vector<int>& citations, int left, int right, int n_to_right) {
    if (left == right) {
      return 0;
    } else if (left + 1 == right) {
      return citations[left] >= n_to_right + 1 ? n_to_right + 1 : 0;
    }
    const int mid = (left + right) / 2;
    const int hLeft = hIndex(citations, left, mid, n_to_right + right - mid);
    if (hLeft > 0) {
      return hLeft;
    }
    return hIndex(citations, mid, right, n_to_right);
  }
};
