#include <cstdlib>
#include <stack>
#include <string>
using namespace std;

class Solution {
 public:
  int calculate(string s) {
    stack<int> nums;
    stack<char> ops;
    for (size_t i = 0; i < s.length() && i != string::npos;) {
      if (s[i] == ' ') {
        ++i;
        continue;
      }
      if (s[i] == '+' || s[i] == '-' || s[i] == '*' || s[i] == '/') {
        ops.push(s[i]);
        ++i;
        continue;
      }
      // Integer.
      const int num = std::atoi(s.c_str() + i);
      if (ops.empty()) {
        nums.push(num);
      } else if (ops.top() == '*') {
        ops.pop();
        nums.top() *= num;
      } else if (ops.top() == '/') {
        ops.pop();
        nums.top() /= num;
      } else {
        nums.push(num);
      }
      i = s.find_first_not_of("0123456789", i);
    }
    int result = 0;
    while (!ops.empty()) {
      if (ops.top() == '+') {
        result += nums.top();
      } else {
        result -= nums.top();
      }
      ops.pop();
      nums.pop();
    }
    return result + nums.top();
  }
};
