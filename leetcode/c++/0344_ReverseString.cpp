#include <string>
using namespace std;

class Solution {
 public:
  string reverseString(string s) {
    int l = 0, r = s.length() - 1;
    while (l < r) {
      char tmp = s[l];
      s[l] = s[r];
      s[r] = tmp;

      ++l;
      --r;
    }
    return s;
  }
};
