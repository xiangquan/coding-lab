struct ListNode {
  int val;
  ListNode* next;
  ListNode(int x) : val(x), next(nullptr) {}
};

class Solution {
 public:
  ListNode* insertionSortList(ListNode* head) {
    if (head == nullptr) {
      return head;
    }
    ListNode* todo = head->next;
    head->next = nullptr;
    while (todo != nullptr) {
      ListNode* next = todo->next;
      if (todo->val < head->val) {
        todo->next = head;
        head = todo;
      } else {
        ListNode* pre = head;
        while (pre->next != nullptr && pre->next->val < todo->val) {
          pre = pre->next;
        }
        todo->next = pre->next;
        pre->next = todo;
      }
      todo = next;
    }
    return head;
  }
};