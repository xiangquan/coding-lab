class Solution {
 public:
  bool isPerfectSquare(int num) {
    int ret = 0;
    for (int i = 15; i >= 0; --i) {
      int mask = 1 << i;
      ret |= mask;

      if (num / ret < ret) {
        ret ^= mask;
      }
    }
    return num / ret == ret && num % ret == 0;
  }
};
