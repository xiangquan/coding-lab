#include <climits>
#include <vector>
using namespace std;

class Solution {
 public:
  vector<vector<int>> kSmallestPairs(vector<int>& nums1, vector<int>& nums2, int k) {
    vector<vector<int>> result;
    vector<int> i2(nums1.size(), 0);
    while (result.size() < k) {
      int cur_min = INT_MAX, cur_index = -1;
      for (int i = 0; i < nums1.size(); ++i) {
        if (i2[i] == nums2.size()) {
          continue;
        }

        int tmp = nums1[i] + nums2[i2[i]];
        if (tmp < cur_min) {
          cur_min = tmp;
          cur_index = i;
        }
      }
      if (cur_index >= 0) {
        result.push_back({nums1[cur_index], nums2[i2[cur_index]]});
        ++i2[cur_index];
      } else {
        break;
      }
    }

    return result;
  }
};
