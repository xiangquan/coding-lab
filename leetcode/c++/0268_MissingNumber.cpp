#include <vector>
using namespace std;

class Solution {
 public:
  int missingNumber(vector<int>& nums) {
    int i = 0;
    for (int j = nums.size(); i < j;) {
      if (nums[i] == i) {
        ++i;
      } else if (nums[i] == nums.size()) {
        swap(nums, i, --j);
      } else {
        swap(nums, i, nums[i]);
      }
    }
    return i;
  }

 private:
  void swap(vector<int>& nums, int i, int j) {
    int tmp = nums[i];
    nums[i] = nums[j];
    nums[j] = tmp;
  }
};
