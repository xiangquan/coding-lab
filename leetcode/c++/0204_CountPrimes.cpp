#include <vector>
using namespace std;

class Solution {
 public:
  int countPrimes(int n) {
    vector<int> primes;
    for (int i = 2; i < n; ++i) {
      bool found = false;
      for (const int prime : primes) {
        if (i / prime < prime) {
          break;
        }

        if (i % prime == 0) {
          found = true;
          break;
        }
      }
      if (!found) {
        primes.push_back(i);
      }
    }
    return primes.size();
  }
};
