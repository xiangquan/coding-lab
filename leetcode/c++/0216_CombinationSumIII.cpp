#include <vector>
using namespace std;

class Solution {
 public:
  vector<vector<int>> combinationSum3(int k, int n) {
    vector<int> prefix;
    vector<vector<int>> result;
    combinationSum3(k, n, 1, 0, &prefix, &result);
    return result;
  }

 private:
  void combinationSum3(const int k, const int n, const int current, const int sum,
                       vector<int>* prefix, vector<vector<int>>* result) {
    if (k == 0) {
      if (n == sum) {
        result->push_back(*prefix);
      }
      return;
    }
    if (sum > n || current > 9) {
      return;
    }
    // Take current.
    prefix->push_back(current);
    combinationSum3(k - 1, n, current + 1, sum + current, prefix, result);
    prefix->pop_back();
    // Or skip current.
    combinationSum3(k, n, current + 1, sum, prefix, result);
  }
};
