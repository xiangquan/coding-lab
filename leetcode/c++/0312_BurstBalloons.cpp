#include <unordered_map>
#include <vector>

using namespace std;

class Solution {
 public:
  int maxCoins(vector<int>& nums) {
    unordered_map<int, int> cache;
    return maxCoins(nums, 0, nums.size(), &cache);
  }

 private:
  // Result of [left, right)
  int maxCoins(const vector<int>& nums, int left, int right, unordered_map<int, int>* cache) {
    // Hit cache.
    const int key = (left << 10) | right;
    const auto iter = cache->find(key);
    if (iter != cache->end()) {
      return iter->second;
    }
    if (left >= right) {
      return 0;
    }

    const int most_left = left == 0 ? 1 : nums[left - 1];
    const int most_right = right == nums.size() ? 1 : nums[right];
    int result = 0;
    for (int i = left; i < right; ++i) {
      const int coin_left = maxCoins(nums, left, i, cache);
      const int coin_right = maxCoins(nums, i + 1, right, cache);
      result = max(result, coin_left + coin_right + most_left * nums[i] * most_right);
    }
    cache->emplace(key, result);

    return result;
  }
};
