#include <vector>
using namespace std;

class Solution {
 public:
  int numIslands(vector<vector<char>>& grid) {
    int result = 0;
    for (int i = 0; i < grid.size(); ++i) {
      for (int j = 0; j < grid[0].size(); ++j) {
        if (grid[i][j] == '1') {
          ++result;
          searchIsland(grid, i, j);
        }
      }
    }
    return result;
  }

 private:
  void searchIsland(vector<vector<char>>& grid, int i, int j) {
    grid[i][j] = 'x';
    if (i > 0 && grid[i - 1][j] == '1') {
      searchIsland(grid, i - 1, j);
    }
    if (j > 0 && grid[i][j - 1] == '1') {
      searchIsland(grid, i, j - 1);
    }
    if (i + 1 < grid.size() && grid[i + 1][j] == '1') {
      searchIsland(grid, i + 1, j);
    }
    if (j + 1 < grid[0].size() && grid[i][j + 1] == '1') {
      searchIsland(grid, i, j + 1);
    }
  }
};
