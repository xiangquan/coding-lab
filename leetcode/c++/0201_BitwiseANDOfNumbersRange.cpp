class Solution {
 public:
  int rangeBitwiseAnd(int m, int n) {
    unsigned int mask = ~0;
    while ((m & mask) != (n & mask)) {
      mask <<= 1;
    }
    return m & mask;
  }
};
