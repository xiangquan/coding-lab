#include <memory>
#include <string>
#include <unordered_map>
#include <vector>
using namespace std;

class Solution {
 public:
  vector<string> findWords(vector<vector<char>>& board, vector<string>& words) {
    for (const string& word : words) {
      trie_tree_.insert(word, 0);
    }

    vector<string> result;
    for (int i = 0; i < board.size(); ++i) {
      for (int j = 0; j < board[0].size(); ++j) {
        string prefix;
        trie_tree_.search(board, i, j, &prefix, &result);
      }
    }
    return result;
  }

 private:
  struct TrieNode {
    bool ending = false;
    unordered_map<char, std::unique_ptr<TrieNode>> children;

    void insert(const string& word, const int index) {
      if (index == word.length()) {
        ending = true;
        return;
      }
      const char c = word[index];
      if (children.find(c) == children.end()) {
        children.emplace(c, new TrieNode);
      }
      children[c]->insert(word, index + 1);
    }

    void search(vector<vector<char>>& board, int i, int j, string* prefix, vector<string>* result) {
      const char c = board[i][j];
      if (children.find(c) == children.end()) {
        return;
      }
      prefix->push_back(c);
      board[i][j] = '.';

      if (children[c]->ending) {
        result->push_back(*prefix);
        children[c]->ending = false;
      }
      if (i > 0) {
        children[c]->search(board, i - 1, j, prefix, result);
      }
      if (j > 0) {
        children[c]->search(board, i, j - 1, prefix, result);
      }
      if (i + 1 < board.size()) {
        children[c]->search(board, i + 1, j, prefix, result);
      }
      if (j + 1 < board[0].size()) {
        children[c]->search(board, i, j + 1, prefix, result);
      }
      board[i][j] = c;
      prefix->pop_back();
    }
  };

  TrieNode trie_tree_;
};
