#include <string>

using std::string;

class Solution {
 public:
  bool isAdditiveNumber(string num) {
    const int longest_int = (num.length() - 1) / 2;
    for (int len1 = 1; len1 <= longest_int; ++len1) {
      if (len1 > 1 && num[0] == '0') {
        return false;
      }

      const auto num1 = num.substr(0, len1);
      const long num1_i = std::atol(num1.c_str());
      for (int len2 = 1; len2 <= longest_int; ++len2) {
        if (len2 > 1 && num[len1] == '0') {
          break;
        }

        const auto num2 = num.substr(len1, len2);
        const long num2_i = std::atol(num2.c_str());
        if (isAdditiveNumber(num, len1 + len2, num1_i, num2_i)) {
          return true;
        }
      }
    }
    return false;
  }

 private:
  bool isAdditiveNumber(const string& num, int start, long num1, long num2) {
    while (start < num.length()) {
      const long sum = num1 + num2;
      const string sum_str = std::to_string(sum);
      if (num.compare(start, sum_str.length(), sum_str) != 0) {
        return false;
      }
      start += sum_str.length();
      num1 = num2;
      num2 = sum;
    }
    return true;
  }
};
