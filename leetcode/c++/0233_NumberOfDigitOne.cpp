class Solution {
 public:
  int countDigitOne(int n) {
    int result = 0;
    long ones_in_lower_rank = 0;
    for (long rank = 1; n / rank; rank *= 10) {
      const int digit = (n / rank) % 10;

      // Add ones in current digit.
      if (digit == 1) {
        result += (n % rank) + 1;
      } else if (digit > 1) {
        result += rank;
      }
      // Add ones in lower rank.
      result += digit * ones_in_lower_rank;
      ones_in_lower_rank = ones_in_lower_rank * 10 + rank;
    }
    return result;
  }
};
