#include <unordered_set>
#include <vector>
using namespace std;

class Solution {
 public:
  bool containsNearbyDuplicate(vector<int>& nums, int k) {
    unordered_set<int> ocu;

    int i = 0, j = 0;
    while (j < nums.size()) {
      if (ocu.find(nums[j]) != ocu.end()) {
        return true;
      }

      ocu.insert(nums[j]);
      if (j - i + 1 > k) {
        ocu.erase(nums[i]);
        ++i;
      }
      ++j;
    }
    return false;
  }
};
