#include <queue>
using namespace std;

class Stack {
 public:
  // Push element x onto stack.
  void push(int x) { to_push.push(x); }

  // Removes the element on top of the stack.
  void pop() {
    queue<int> tmp;
    while (to_push.size() > 1) {
      tmp.push(to_push.front());
      to_push.pop();
    }
    to_push.swap(tmp);
  }

  // Get the top element.
  int top() {
    queue<int> tmp;
    while (to_push.size() > 1) {
      tmp.push(to_push.front());
      to_push.pop();
    }
    int ret = to_push.front();
    tmp.push(ret);
    to_push.swap(tmp);
    return ret;
  }

  // Return whether the stack is empty.
  bool empty() { return to_push.empty(); }

 private:
  queue<int> to_push;
};
