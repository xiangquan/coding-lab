#include <vector>

using std::vector;

class NumArray {
 public:
  NumArray(vector<int>& nums) : nums_(nums), sums_(nums.begin(), nums.end()) {
    for (int i = 1; i < nums.size(); ++i) {
      sums_[i] += sums_[i - 1];
    }
  }

  void update(int i, int val) {
    const long diff = val - nums_[i];
    nums_[i] = val;
    for (; i < sums_.size(); ++i) {
      sums_[i] += diff;
    }
  }

  int sumRange(int i, int j) { return i > 0 ? sums_[j] - sums_[i - 1] : sums_[j]; }

 private:
  vector<int> nums_;
  vector<long> sums_;
};
