#include <algorithm>
#include <stack>
using namespace std;

class MinStack {
 public:
  void push(int x) {
    nums.push(x);
    if (mins.empty()) {
      mins.push(x);
    } else {
      mins.push(std::min(x, mins.top()));
    }
  }

  void pop() {
    nums.pop();
    mins.pop();
  }

  int top() { return nums.top(); }

  int getMin() { return mins.top(); }

 private:
  stack<int> nums;
  stack<int> mins;
};

/**
 * Your MinStack object will be instantiated and called as such:
 * MinStack obj = new MinStack();
 * obj.push(x);
 * obj.pop();
 * int param_3 = obj.top();
 * int param_4 = obj.getMin();
 */
