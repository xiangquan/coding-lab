#include <algorithm>

// Definition for binary tree
struct TreeNode {
  int val;
  TreeNode *left;
  TreeNode *right;
  TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
};

class Solution {
 public:
  int maxPathSum(TreeNode *root) {
    if (root == nullptr) {
      return 0;
    }
    int result = root->val;
    int sumWithRoot = maxPathSum(root, result);
    return std::max(sumWithRoot, result);
  }

 private:
  int maxPathSum(TreeNode *root, int &result) {
    // return a mini number, make sure it <= result
    if (root == nullptr) {
      return result < 0 ? result : -1;
    }

    const int left = maxPathSum(root->left, result);
    const int right = maxPathSum(root->right, result);

    const int maxiChild = std::max(left, right);
    const int withRoot = left + right + root->val;
    result = std::max(result, std::max(maxiChild, withRoot));

    if (left <= 0 && right <= 0) {
      return root->val;
    }
    return maxiChild + root->val;
  }
};
