#include <algorithm>
#include <string>
#include <vector>
using namespace std;

class Solution {
 public:
  string largestNumber(vector<int>& nums) {
    vector<string> strNums;
    strNums.reserve(nums.size());
    for (const int num : nums) {
      strNums.push_back(std::to_string(num));
    }
    std::sort(strNums.begin(), strNums.end(), cmpByDigits);
    string result;
    for (const string& num : strNums) {
      result += num;
    }
    while (result.length() > 1 && result[0] == '0') {
      result.erase(0, 1);
    }
    return result;
  }

 private:
  static bool cmpByDigits(const string& num1, const string& num2) {
    for (size_t i = 0; i < num1.length(); ++i) {
      if (i == num2.length()) {
        return cmpByDigits(num1.substr(i), num2);
      }
      if (num1[i] == num2[i]) {
        continue;
      }
      return num1[i] > num2[i];
    }
    if (num1.length() == num2.length()) {
      return false;
    }
    return cmpByDigits(num1, num2.substr(num1.length()));
  }
};
