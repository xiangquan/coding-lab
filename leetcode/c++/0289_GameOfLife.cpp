#include <vector>
using namespace std;

class Solution {
 public:
  void gameOfLife(vector<vector<int>>& board) {
    for (int i = 0; i < board.size(); ++i) {
      for (int j = 0; j < board[0].size(); ++j) {
        int live_neighbors = 0;
        for (int row = -1; row <= 1; ++row) {
          for (int col = -1; col <= 1; ++col) {
            if (row == 0 && col == 0) {
              continue;
            }
            int x = i + row, y = j + col;
            if (x < 0 || y < 0 || x == board.size() || y == board[0].size()) {
              continue;
            }
            if (board[x][y] > 0) {
              ++live_neighbors;
            }
          }
        }

        if (board[i][j] == 1) {
          if (live_neighbors < 2 || live_neighbors > 3) {
            // live to dead
            board[i][j] = 2;
          }
        } else {
          if (live_neighbors == 3) {
            // dead to live
            board[i][j] = -1;
          }
        }
      }
    }
    for (int i = 0; i < board.size(); ++i) {
      for (int j = 0; j < board[0].size(); ++j) {
        if (board[i][j] == -1) {
          board[i][j] = 1;
        } else if (board[i][j] == 2) {
          board[i][j] = 0;
        }
      }
    }
  }
};
