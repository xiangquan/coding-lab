class Solution {
 public:
  int trailingZeroes(int n) {
    int exp5 = 5;
    int result = 0;

    while (true) {
      int tmp = n / exp5;
      result += tmp;
      if (tmp < 5) {
        return result;
      }
      exp5 *= 5;
    }
    return 0;
  }
};
