// Forward declaration of isBadVersion API.
bool isBadVersion(int version);

class Solution {
 public:
  int firstBadVersion(int n) {
    int l = 0, r = n;
    while (l + 1 < r) {
      int mid = l + (r - l) / 2;
      if (isBadVersion(mid + 1)) {
        r = mid;
      } else {
        l = mid;
      }
    }

    return isBadVersion(l + 1) ? l + 1 : r + 1;
  }
};
