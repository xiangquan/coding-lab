#include <algorithm>
#include <vector>
using namespace std;

class Solution {
 public:
  int rob(vector<int>& nums) {
    if (nums.empty()) {
      return 0;
    }
    if (nums.size() == 1) {
      return nums[0];
    }

    vector<int> best(nums.size(), 0);
    best[0] = nums[0];
    best[1] = std::max(nums[0], nums[1]);
    for (int i = 2; i < nums.size(); ++i) {
      best[i] = std::max(best[i - 1], best[i - 2] + nums[i]);
    }
    return best[nums.size() - 1];
  }
};
