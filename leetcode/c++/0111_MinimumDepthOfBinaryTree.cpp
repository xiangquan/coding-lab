#include <queue>
using namespace std;

// Definition for binary tree
struct TreeNode {
  int val;
  TreeNode *left;
  TreeNode *right;
  TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
};

class SimpleSolution {
 public:
  int minDepth(TreeNode *root) {
    if (root == nullptr) {
      return 0;
    } else if (root->left == nullptr && root->right == nullptr) {
      return 1;
    } else if (root->left == nullptr) {
      return minDepth(root->right) + 1;
    } else if (root->right == nullptr) {
      return minDepth(root->left) + 1;
    }
    return std::min(minDepth(root->left), minDepth(root->right)) + 1;
  }
};

class Solution {
 public:
  int minDepth(TreeNode *root) {
    if (root == nullptr) {
      return 0;
    }
    queue<TreeNode *> toVisit;
    toVisit.push(root);
    toVisit.push(nullptr);
    int level = 1;
    while (true) {
      TreeNode *cur = toVisit.front();
      toVisit.pop();
      if (cur == nullptr) {
        ++level;
        toVisit.push(nullptr);
      } else {
        if (cur->left == nullptr && cur->right == nullptr) {
          return level;
        }
        if (cur->left) {
          toVisit.push(cur->left);
        }
        if (cur->right) {
          toVisit.push(cur->right);
        }
      }
    }
    return level;
  }
};
