#include <algorithm>
#include <vector>
using namespace std;

class Solution {
 public:
  int minSubArrayLen(int s, vector<int>& nums) {
    int l = 0, r = 0, sum = 0, min_len = INT_MAX;
    while (r < nums.size()) {
      sum += nums[r++];
      if (sum >= s) {
        // Move l
        while (sum >= s) {
          sum -= nums[l++];
        }
        min_len = std::min(min_len, r - l + 1);
      }
    }
    return min_len == INT_MAX ? 0 : min_len;
  }
};
