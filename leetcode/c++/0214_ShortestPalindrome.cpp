#include <algorithm>
#include <string>
using namespace std;

class Solution {
 public:
  string shortestPalindrome(string s) {
    if (s.length() <= 1) {
      return s;
    }

    int l = 0;
    for (int r = s.length() - 1; r >= 0; --r) {
      if (s[l] == s[r]) {
        ++l;
      }
    }
    if (l == s.length()) {
      return s;
    }

    string suffix = s.substr(l);
    string prefix = suffix;
    std::reverse(prefix.begin(), prefix.end());
    return prefix + shortestPalindrome(s.substr(0, l)) + suffix;
  }
};
