#include <stack>
#include <string>
#include <vector>
using namespace std;

class Solution {
 public:
  int evalRPN(vector<string>& tokens) {
    stack<int> todo;
    for (const string& token : tokens) {
      if (token == "+" || token == "-" || token == "*" || token == "/") {
        const int num1 = todo.top();
        todo.pop();
        const int num2 = todo.top();
        todo.pop();
        if (token == "+") {
          todo.push(num2 + num1);
        } else if (token == "-") {
          todo.push(num2 - num1);
        } else if (token == "*") {
          todo.push(num2 * num1);
        } else {
          todo.push(num2 / num1);
        }
      } else {
        todo.push(atoi(token.c_str()));
      }
    }
    return todo.top();
  }
};
