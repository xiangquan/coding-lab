#include <stack>
using namespace std;

struct ListNode {
  int val;
  ListNode* next;
  ListNode(int x) : val(x), next(nullptr) {}
};

class Solution {
 public:
  void reorderList(ListNode* head) {
    stack<ListNode*> nodes;
    for (ListNode* tail = head; tail != nullptr; tail = tail->next) {
      nodes.push(tail);
    }
    for (ListNode* tail = head; tail != nullptr; tail = tail->next) {
      ListNode* next = nodes.top();
      if (tail == next) {
        tail->next = nullptr;
        return;
      }
      if (tail->next == next) {
        next->next = nullptr;
        return;
      }
      next->next = tail->next;
      tail->next = next;
      tail = next;
      nodes.pop();
    }
  }
};
