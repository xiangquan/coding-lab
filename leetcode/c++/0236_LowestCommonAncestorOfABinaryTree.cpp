#include <vector>
using namespace std;

struct TreeNode {
  int val;
  TreeNode* left;
  TreeNode* right;
  TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
};

class Solution {
 public:
  TreeNode* lowestCommonAncestor(TreeNode* root, TreeNode* p, TreeNode* q) {
    if (root == nullptr) {
      return nullptr;
    }
    vector<TreeNode*> path, path_to_p, path_to_q;
    pathToNode(root, p, q, &path, &path_to_p, &path_to_q);
    int i = 0;
    for (; i < path_to_p.size() && i < path_to_q.size(); ++i) {
      if (path_to_p[i] != path_to_q[i]) {
        break;
      }
    }
    return path_to_p[i - 1];
  }

 private:
  void pathToNode(TreeNode* root, TreeNode* p, TreeNode* q, vector<TreeNode*>* path,
                  vector<TreeNode*>* path_to_p, vector<TreeNode*>* path_to_q) {
    if (root == nullptr) {
      return;
    }
    path->push_back(root);
    if (root == p) {
      *path_to_p = *path;
    }
    if (root == q) {
      *path_to_q = *path;
    }
    if (!path_to_p->empty() && !path_to_q->empty()) {
      return;
    }
    pathToNode(root->left, p, q, path, path_to_p, path_to_q);
    pathToNode(root->right, p, q, path, path_to_p, path_to_q);
    path->pop_back();
  }
};
