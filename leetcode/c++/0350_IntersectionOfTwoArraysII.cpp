#include <unordered_map>
#include <vector>
using namespace std;

class Solution {
 public:
  vector<int> intersect(vector<int>& nums1, vector<int>& nums2) {
    unordered_map<int, int> count1;
    for (const int num : nums1) {
      if (count1.find(num) == count1.end()) {
        count1[num] = 1;
      } else {
        ++count1[num];
      }
    }
    unordered_map<int, int> result;
    for (const int num : nums2) {
      if (count1.find(num) != count1.end()) {
        if (result.find(num) == result.end()) {
          result[num] = 1;
        } else {
          ++result[num];
        }
        if (--count1[num] == 0) {
          count1.erase(num);
        }
      }
    }
    vector<int> ret;
    for (const auto& entry : result) {
      for (int i = 0; i < entry.second; ++i) {
        ret.push_back(entry.first);
      }
    }
    return ret;
  }
};
