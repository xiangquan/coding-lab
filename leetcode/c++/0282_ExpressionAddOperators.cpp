#include <set>
#include <string>
#include <tuple>
#include <vector>
using namespace std;

class Solution {
 public:
  vector<string> addOperators(string num, int target) {
    vector<string> result;
    addOperators(num, target, 0, "", 0, 0, &result);
    return result;
  }

 private:
  void addOperators(const string& num, const int target, int pos, const string& expr,
                    const long value, const long prev_operand, vector<string>* result) {
    if (pos == num.length()) {
      if (value == target) {
        result->push_back(expr);
      }
      return;
    }

    for (int len = 1; pos + len <= num.size(); ++len) {
      // Invalid integer.
      if (len > 1 && num[pos] == '0') {
        break;
      }
      // Parse integer.
      const string operand_s = num.substr(pos, len);
      const long operand = std::stol(operand_s);
      if (operand > INT_MAX) {
        break;
      }

      string new_expr;
      long new_value, new_operand;
      if (pos == 0) {
        new_expr = operand_s;
        new_value = operand;
        new_operand = operand;
        addOperators(num, target, pos + len, new_expr, new_value, new_operand, result);
      } else {
        new_expr = expr + '+' + operand_s;
        new_value = value + operand;
        new_operand = operand;
        addOperators(num, target, pos + len, new_expr, new_value, new_operand, result);

        new_expr = expr + '-' + operand_s;
        new_value = value - operand;
        new_operand = -operand;
        addOperators(num, target, pos + len, new_expr, new_value, new_operand, result);

        new_expr = expr + '*' + operand_s;
        new_value = value - prev_operand + prev_operand * operand;
        new_operand = prev_operand * operand;
        addOperators(num, target, pos + len, new_expr, new_value, new_operand, result);
      }
    }
  }
};
