#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>
using namespace std;

class Solution {
 public:
  vector<string> removeInvalidParentheses(string s) {
    unordered_map<string, unordered_set<string>> cache;
    const unordered_set<string>& result = removeInvalidParentheses(s, 0, s.length(), &cache);
    return {result.begin(), result.end()};
  }

 private:
  // Result of s[left, right)
  const unordered_set<string>& removeInvalidParentheses(
      const string& s, int left, int right, unordered_map<string, unordered_set<string>>* cache) {
    // Skip leading ')' end trailing '('.
    while (left < right && s[left] == ')') {
      ++left;
    }
    while (left < right && s[right - 1] == '(') {
      --right;
    }
    const string part = s.substr(left, right - left);
    if (cache->find(part) != cache->end()) {
      return cache->at(part);
    }
    cache->emplace(part, unordered_set<string>());
    unordered_set<string>* result = &cache->at(part);
    if (left + 1 >= right) {
      result->insert(part);  // Either empty or a letter.
      return *result;
    }

    // "(...)"
    int max_length = 0;
    if (part[0] == '(' && part.back() == ')') {
      const auto& result_inside = removeInvalidParentheses(part, 1, part.length() - 1, cache);
      for (const string& part_result : result_inside) {
        result->insert("(" + part_result + ")");
      }
      max_length = result->begin()->length();
    }

    // "...|..."
    for (int i = 1; i < part.length(); ++i) {
      const auto& result_left = removeInvalidParentheses(part, 0, i, cache);
      const auto& result_right = removeInvalidParentheses(part, i, part.length(), cache);
      const int new_length = result_left.begin()->length() + result_right.begin()->length();
      if (new_length < max_length) {
        continue;
      }
      if (new_length > max_length) {
        result->clear();
        max_length = new_length;
      }
      for (const string& part_left : result_left) {
        for (const string& part_right : result_right) {
          result->insert(part_left + part_right);
        }
      }
    }

    return *result;
  }
};
