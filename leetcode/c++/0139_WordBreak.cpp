#include <string>
#include <unordered_set>
#include <vector>
using namespace std;

class Solution {
 public:
  bool wordBreak(string s, vector<string>& wordDict) {
    const unordered_set<string> words(wordDict.begin(), wordDict.end());
    // -1: unknown
    // 0: false
    // 1: true
    vector<int> mem(s.length() + 1, -1);
    mem.back() = 1;
    return IsWordBreakable(s, 0, words, &mem);
  }

 private:
  bool IsWordBreakable(const string& s, int index, const unordered_set<string>& wordDict,
                       vector<int>* mem) {
    int& index_mem = mem->at(index);
    if (index_mem == -1) {
      for (int len = 1; len <= int(s.length()) - index; ++len) {
        string word = s.substr(index, len);
        if (wordDict.find(word) != wordDict.end() &&
            IsWordBreakable(s, index + len, wordDict, mem)) {
          index_mem = 1;
          break;
        }
      }
      if (index_mem == -1) {
        index_mem = 0;
      }
    }
    return index_mem == 1;
  }
};
