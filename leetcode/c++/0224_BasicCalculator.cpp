#include <cstdlib>
#include <stack>
#include <string>
using namespace std;

class Solution {
 public:
  int calculate(string s) {
    stack<int> nums;
    stack<char> ops;
    for (size_t i = 0; i < s.length() && i != string::npos;) {
      if (s[i] == ' ') {
        ++i;
        continue;
      }
      if (s[i] == '+' || s[i] == '-' || s[i] == '(') {
        ops.push(s[i]);
        ++i;
        continue;
      }
      if (s[i] == ')') {
        ops.pop();  // '('

        if (!ops.empty() && (ops.top() == '+' || ops.top() == '-')) {
          const int num = nums.top();
          nums.pop();
          if (ops.top() == '+') {
            nums.top() += num;
          } else {
            nums.top() -= num;
          }
          ops.pop();
        }
        ++i;
        continue;
      }
      // Integer.
      const int num = std::atoi(s.c_str() + i);
      if (ops.empty()) {
        nums.push(num);
      } else if (ops.top() == '+') {
        ops.pop();
        nums.top() += num;
      } else if (ops.top() == '-') {
        ops.pop();
        nums.top() -= num;
      } else {
        nums.push(num);
      }
      i = s.find_first_not_of("0123456789", i);
    }
    return nums.top();
  }
};
