#include <unordered_set>
#include <vector>
using namespace std;

class Solution {
 public:
  bool containsDuplicate(vector<int>& nums) {
    unordered_set<int> dict;
    for (const int num : nums) {
      if (dict.find(num) != dict.end()) {
        return true;
      }
      dict.insert(num);
    }
    return false;
  }
};
