class Solution {
 public:
  bool isPowerOfFour(int num) {
    if (num <= 0) {
      return false;
    }

    int n = 0;
    while (true) {
      if (num == 0) {
        return false;
      }
      if (num == 1) {
        return n % 2 == 0;
      }
      if (num & 0x1) {
        return false;
      }
      num >>= 1;
      ++n;
    }
    return false;
  }
};
