#include <string>
#include <unordered_map>
using namespace std;

class Solution {
 public:
  string getHint(string secret, string guess) {
    unordered_map<char, int> count;
    for (const char c : secret) {
      if (count.find(c) == count.end()) {
        count[c] = 1;
      } else {
        ++count[c];
      }
    }

    int bulls = 0, cows = 0;
    for (int i = 0; i < guess.length(); ++i) {
      if (secret[i] == guess[i]) {
        ++bulls;
        if (--count[guess[i]] < 0) {
          --cows;
        }
      } else if (count.find(guess[i]) != count.end()) {
        if (--count[guess[i]] >= 0) {
          ++cows;
        }
      }
    }
    char buf[128];
    sprintf(buf, "%dA%dB", bulls, cows);
    return buf;
  }
};
