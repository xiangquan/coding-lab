// Definition for binary tree
struct TreeNode {
  int val;
  TreeNode *left;
  TreeNode *right;
  TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
};

class Solution {
 public:
  int sumNumbers(TreeNode *root) { return sumNumbers(root, 0); }

 private:
  int sumNumbers(TreeNode *root, int parent) {
    if (root == nullptr) {
      return 0;
    }

    parent = parent * 10 + root->val;
    if (root->left == nullptr && root->right == nullptr) {
      return parent;
    }

    return sumNumbers(root->left, parent) + sumNumbers(root->right, parent);
  }
};
