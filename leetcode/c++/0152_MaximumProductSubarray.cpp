#include <algorithm>
#include <vector>
using namespace std;

class Solution {
 public:
  int maxProduct(vector<int>& nums) {
    if (nums.empty()) {
      return 0;
    }

    int min_left = nums[0];
    int max_left = nums[0];
    int result = max_left;
    for (int i = 1; i < nums.size(); ++i) {
      if (nums[i] < 0) {
        const int min_l = min_left;
        min_left = std::min(nums[i], max_left * nums[i]);
        max_left = std::max(nums[i], min_l * nums[i]);
      } else {
        min_left = std::min(nums[i], min_left * nums[i]);
        max_left = std::max(nums[i], max_left * nums[i]);
      }
      result = std::max(result, max_left);
    }

    return result;
  }
};
