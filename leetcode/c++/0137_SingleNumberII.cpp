#include <unordered_map>
#include <vector>
using namespace std;

class Solution {
 public:
  int singleNumber(vector<int>& nums) {
    unordered_map<int, bool> mem;
    for (const int num : nums) {
      mem[num] = (mem.find(num) == mem.end());
    }
    for (const auto iter : mem) {
      if (iter.second) {
        return iter.first;
      }
    }
    return 0;
  }
};
