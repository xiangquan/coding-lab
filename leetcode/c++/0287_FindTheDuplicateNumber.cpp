#include <tuple>
#include <vector>
using namespace std;

class Solution {
 public:
  int findDuplicate(vector<int>& nums) {
    int i = 1, j = nums.size() - 1;
    while (i < j) {
      const int mid = (i + j) / 2;
      const int left_count = std::count_if(
          nums.begin(), nums.end(), [i, mid](const int num) { return num >= i && num <= mid; });
      if (left_count > mid - i + 1) {
        j = mid;
      } else {
        i = mid + 1;
      }
    }
    return i;
  }
};
