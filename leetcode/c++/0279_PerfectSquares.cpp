class Solution {
 public:
  int numSquares(int n) {
    if (n <= 0) {
      return 0;
    }
    int result = INT_MAX;
    int current_pick = 1, current_sum = 0, current_count = 0;
    numSquares(n, current_pick, current_sum, current_count, &result);
    return result;
  }

 private:
  void numSquares(const int target_sum, const int current_pick, const int current_sum,
                  const int current_count, int* result) {
    if (current_count >= *result) {
      return;
    }
    if (current_sum == target_sum) {
      *result = current_count;
      return;
    }
    // Skip current pick.
    const int next_pick = current_pick + 1;
    if ((target_sum - current_sum) / next_pick >= next_pick) {
      numSquares(target_sum, next_pick, current_sum, current_count, result);
    }
    // Take current pick.
    if ((target_sum - current_sum) / current_pick >= current_pick) {
      numSquares(target_sum, current_pick, current_sum + current_pick * current_pick,
                 current_count + 1, result);
    }
  }
};
