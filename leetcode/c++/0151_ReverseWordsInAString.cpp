#include <string>
using namespace std;

class Solution {
 public:
  string reverseWords(string s) {
    string result;
    for (size_t start = 0; start < s.length();) {
      const size_t next_start = s.find_first_not_of(' ', start);
      if (next_start == string::npos) {
        break;
      }
      size_t next_end = s.find_first_of(' ', next_start);
      if (next_end == string::npos) {
        next_end = s.length();
      }

      const string word = s.substr(next_start, next_end - next_start);
      if (result.empty()) {
        result = word;
      } else {
        result = word + " " + result;
      }
      start = next_end;
    }
    return result;
  }
};
