#include <string>
#include <unordered_map>
using namespace std;

class Solution {
 public:
  string numberToWords(int num) {
    static constexpr int kBillion = 1000000000;
    static constexpr int kMillion = 1000000;
    static constexpr int kThousand = 1000;

    if (num == 0) {
      return "Zero";
    }

    string result;
    if (num >= kBillion) {
      result = hundredNumberToWords(num / kBillion) + " Billion";
      num %= kBillion;
    }

    if (num >= kMillion) {
      if (!result.empty()) {
        result += " ";
      }
      result += hundredNumberToWords(num / kMillion) + " Million";
      num %= kMillion;
    }

    if (num >= kThousand) {
      if (!result.empty()) {
        result += " ";
      }
      result += hundredNumberToWords(num / kThousand) + " Thousand";
      num %= kThousand;
    }

    if (num > 0) {
      if (!result.empty()) {
        result += " ";
      }
      result += hundredNumberToWords(num);
    }
    return result;
  }

 private:
  static string hundredNumberToWords(int num) {
    static unordered_map<int, string> kWords = {
        {1, "One"},      {2, "Two"},        {3, "Three"},     {4, "Four"},      {5, "Five"},
        {6, "Six"},      {7, "Seven"},      {8, "Eight"},     {9, "Nine"},      {10, "Ten"},
        {11, "Eleven"},  {12, "Twelve"},    {13, "Thirteen"}, {14, "Fourteen"}, {15, "Fifteen"},
        {16, "Sixteen"}, {17, "Seventeen"}, {18, "Eighteen"}, {19, "Nineteen"}, {20, "Twenty"},
        {30, "Thirty"},  {40, "Forty"},     {50, "Fifty"},    {60, "Sixty"},    {70, "Seventy"},
        {80, "Eighty"},  {90, "Ninety"},
    };

    string result;
    if (num >= 100) {
      result = kWords[num / 100] + " Hundred";
      num %= 100;
    }
    if (num == 0) {
      return result;
    }
    if (!result.empty()) {
      result += " ";
    }
    if (num <= 20 || num % 10 == 0) {
      result += kWords[num];
    } else {
      result += kWords[num / 10 * 10] + " " + kWords[num % 10];
    }
    return result;
  }
};
