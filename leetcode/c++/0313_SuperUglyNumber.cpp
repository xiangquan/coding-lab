#include <algorithm>
#include <set>
#include <vector>

using namespace std;

class Solution {
 public:
  int nthSuperUglyNumber(int n, vector<int>& primes) {
    std::sort(primes.begin(), primes.end());

    set<unsigned long> candidates(primes.begin(), primes.end());
    candidates.insert(1);
    while (--n) {
      const auto min_iter = candidates.begin();
      const unsigned long min = *min_iter;
      std::cout << min << std::endl;
      candidates.erase(min_iter);
      for (const unsigned long prime : primes) {
        const unsigned long candidate = min * prime;
        unsigned long current_max = 0;
        const bool overflowed = candidates.size() == n;
        if (overflowed) {
          current_max = *candidates.rbegin();
          if (candidate >= current_max) {
            break;
          }
        }
        if (candidates.insert(candidate).second && overflowed) {
          candidates.erase(current_max);
        }
      }
    }
    return *candidates.begin();
  }
};
