#include <stack>
#include <vector>
using namespace std;

// Definition for a binary tree node.
struct TreeNode {
  int val;
  TreeNode* left;
  TreeNode* right;
  TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
};

class Solution {
 public:
  vector<int> preorderTraversal(TreeNode* root) {
    vector<int> result;
    if (!root) {
      return result;
    }

    stack<TreeNode*> todo;
    todo.push(root);
    while (!todo.empty()) {
      TreeNode* node = todo.top();
      todo.pop();
      result.push_back(node->val);
      if (node->right) {
        todo.push(node->right);
      }
      if (node->left) {
        todo.push(node->left);
      }
    }
    return result;
  }
};
