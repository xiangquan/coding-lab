#include <string>
#include <unordered_map>
using namespace std;

class Solution {
 public:
  bool isAnagram(string s, string t) {
    if (s.length() != t.length()) {
      return false;
    }

    unordered_map<char, int> count;
    for (const char ch : s) {
      if (count.find(ch) == count.end()) {
        count[ch] = 1;
      } else {
        ++count[ch];
      }
    }

    for (const char ch : t) {
      if (count.find(ch) == count.end()) {
        return false;
      }

      if (--count[ch] < 0) {
        return false;
      }
    }
    return true;
  }
};
