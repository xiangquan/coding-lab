#include <map>
#include <tuple>
#include <unordered_set>
#include <vector>
using namespace std;

class Solution {
 public:
  int maxPoints(vector<vector<int>>& points) {
    if (points.size() <= 2) {
      return points.size();
    }

    // lines[a,b,c,d] = {point, ...}
    // represents the line y = (b/a)x + (d/c)
    map<tuple<long, long, long, long>, unordered_set<int>> lines;

    int max_points = 0;
    for (int i = 0; i + 1 < points.size(); ++i) {
      for (int j = i + 1; j < points.size(); ++j) {
        const tuple<long, long, long, long> key = calcLine(points[i], points[j]);
        auto iter = lines.find(key);
        if (iter == lines.end()) {
          iter = lines.emplace(key, unordered_set<int>()).first;
        }
        iter->second.insert(i);
        iter->second.insert(j);
        max_points = std::max<int>(max_points, iter->second.size());
      }
    }
    return max_points;
  }

 private:
  tuple<long, long, long, long> calcLine(const vector<int>& p1, const vector<int>& p2) {
    long a, b, c, d;
    a = p1[0] - p2[0];
    b = p1[1] - p2[1];
    if (a == 0) {
      b = p1[0];
      c = d = 0;
    } else if (b == 0) {
      a = 1;
      c = 1;
      d = p1[1];
    } else {
      reduct(&a, &b);
      d = a * p1[1] - b * p1[0];
      if (d == 0) {
        c = 1;
      } else {
        c = a;
        reduct(&c, &d);
      }
    }
    return std::make_tuple(a, b, c, d);
  }

  void reduct(long* a, long* b) {
    const bool neg_a = *a < 0;
    const bool neg_b = *b < 0;
    if (neg_a) {
      *a = -(*a);
    }
    if (neg_b) {
      *b = -(*b);
    }

    for (int i = std::min(*a, *b); i > 1; --i) {
      if (*a % i == 0 && *b % i == 0) {
        *a /= i;
        *b /= i;
        break;
      }
    }

    if (neg_a ^ neg_b) {
      *a = -(*a);
    }
  }
};
