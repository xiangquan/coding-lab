#include <vector>
using namespace std;

// Below is the interface for Iterator, which is already defined for you.
class Iterator {
 public:
  Iterator(const vector<int>& nums);
  Iterator(const Iterator& iter);
  virtual ~Iterator();
  int next();
  bool hasNext() const;
};

class PeekingIterator : public Iterator {
 public:
  PeekingIterator(const vector<int>& nums) : Iterator(nums) {}

  int peek() {
    if (!has_peeked) {
      peeked = Iterator::next();
      has_peeked = true;
    }
    return peeked;
  }

  int next() {
    if (has_peeked) {
      has_peeked = false;
      return peeked;
    }
    return Iterator::next();
  }

  bool hasNext() const { return has_peeked || Iterator::hasNext(); }

 private:
  int peeked;
  bool has_peeked = false;
};
