#include <vector>
using namespace std;

class Solution {
 public:
  int canCompleteCircuit(vector<int>& gas, vector<int>& cost) {
    int tank = 0;
    for (int i = 2 * gas.size() - 1; i >= 0; --i) {
      int idx = i % gas.size();
      tank -= cost[idx] - gas[idx];
      if (tank >= 0) {
        if (i < gas.size()) {
          return i;
        }
        tank = 0;
      }
    }
    return -1;
  }
};
