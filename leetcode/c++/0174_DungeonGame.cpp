#include <limits>
#include <set>
#include <tuple>
#include <vector>
using namespace std;

class Solution {
 public:
  typedef tuple<int, int> Path;  // {current_sum, min_on_path}
  typedef set<Path, std::greater<Path>> Paths;

  int calculateMinimumHP(vector<vector<int>>& dungeon) {
    vector<vector<Paths>> paths(dungeon.size(), vector<Paths>(dungeon[0].size(), Paths()));
    for (size_t row = 0; row < dungeon.size(); ++row) {
      for (size_t col = 0; col < dungeon[0].size(); ++col) {
        if (row == 0 && col == 0) {
          paths[row][col].insert(std::make_tuple(dungeon[row][col], dungeon[row][col]));
        }
        if (row > 0) {
          moveFrom(paths[row - 1][col], dungeon[row][col], &paths[row][col]);
        }
        if (col > 0) {
          moveFrom(paths[row][col - 1], dungeon[row][col], &paths[row][col]);
        }
      }
    }
    int max_min_on_path = std::numeric_limits<int>::min();
    for (const Path& path : paths.back().back()) {
      max_min_on_path = std::max(max_min_on_path, std::get<1>(path));
    }
    return std::max(1, 1 - max_min_on_path);
  }

 private:
  void moveFrom(const Paths& from, const int cell_value, Paths* to) {
    int max_min_on_path = std::numeric_limits<int>::min();
    for (const Path& path : from) {
      int current_sum, min_on_path;
      std::tie(current_sum, min_on_path) = path;
      if (min_on_path <= max_min_on_path) {
        continue;
      }
      max_min_on_path = std::max(max_min_on_path, min_on_path);
      current_sum += cell_value;
      to->insert(std::make_tuple(current_sum, std::min(min_on_path, current_sum)));
    }
  }
};
