class Solution {
 public:
  int countNumbersWithUniqueDigits(int n) {
    if (n <= 0) {
      return 1;
    }
    return count(n, 0);
  }

  int count(int remain, int taken) {
    if (remain == 1) {
      return 10 - taken;
    }
    if (taken == 10) {
      return 0;
    }

    if (taken == 0) {
      return 9 * count(remain - 1, 1) + count(remain - 1, 0);
    }
    return (10 - taken) * count(remain - 1, taken + 1);
  }
};
