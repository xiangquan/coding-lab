#include <algorithm>
#include <functional>
#include <tuple>
#include <unordered_set>
#include <vector>
using namespace std;

class Solution {
 public:
  // {ugly, a, b, c} where ugly = 2a+3b+5c
  typedef tuple<unsigned long, unsigned long, unsigned long, unsigned long> Elem;

  int nthUglyNumber(int n) {
    if (n <= 0) {
      return 0;
    }

    vector<Elem> heap;
    heap.emplace_back(1, 0, 0, 0);
    unordered_set<unsigned long> ugly_cache;

    while (--n) {
      unsigned long ugly, a, b, c;
      std::tie(ugly, a, b, c) = heap[0];
      std::pop_heap(heap.begin(), heap.end(), std::greater<Elem>());
      heap.pop_back();

      addCandidate(ugly * 2, a + 1, b, c, &heap, &ugly_cache);
      addCandidate(ugly * 3, a, b + 1, c, &heap, &ugly_cache);
      addCandidate(ugly * 5, a, b, c + 1, &heap, &ugly_cache);
    }
    return std::get<0>(heap[0]);
  }

 private:
  static void addCandidate(const unsigned long ugly, const unsigned long a, const unsigned long b,
                           const unsigned long c, vector<Elem>* heap,
                           unordered_set<unsigned long>* ugly_cache) {
    if (ugly_cache->insert(ugly).second) {
      heap->emplace_back(ugly, a, b, c);
      std::push_heap(heap->begin(), heap->end(), std::greater<Elem>());
    }
  }
};
