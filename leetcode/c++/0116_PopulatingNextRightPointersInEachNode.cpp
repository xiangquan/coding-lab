// Definition for binary tree with next pointer.
struct Node {
  int val;
  Node *left, *right, *next;
  Node(int x) : val(x), left(nullptr), right(nullptr), next(nullptr) {}
};

class SimpleSolution {
 public:
  Node* connect(Node* root) {
    if (root == nullptr || root->left == nullptr) {
      return root;
    }
    Node* left = root->left;
    Node* right = root->right;
    connect(left);
    connect(right);
    while (left != nullptr) {
      left->next = right;
      left = left->right;
      right = right->left;
    }
    return root;
  }
};

class Solution {
 public:
  Node* connect(Node* root) {
    Node* node = root;
    while (node != nullptr) {
      // connect one level and return the head of next level
      Node* nextLevel = node->left;
      while (node != nullptr) {
        Node *left = node->left, *right = node->right;
        while (left != nullptr) {
          left->next = right;
          left = left->right;
          right = right->left;
        }
        node = node->next;
      }
      node = nextLevel;
    }
    return root;
  }
};
