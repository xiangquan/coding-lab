#include <algorithm>
#include <functional>
#include <vector>
using namespace std;

class Solution {
 public:
  int findKthLargest(vector<int>& nums, int k) {
    std::make_heap(nums.begin(), nums.end());
    for (int i = 0; i + 1 < k; ++i) {
      std::pop_heap(nums.begin(), nums.end() - i);
    }
    return nums[0];
  }
};
