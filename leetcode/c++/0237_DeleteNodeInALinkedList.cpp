// Definition for singly-linked list.
struct ListNode {
  int val;
  ListNode* next;
  ListNode(int x) : val(x), next(nullptr) {}
};

class Solution {
 public:
  void deleteNode(ListNode* node) {
    while (node && node->next && node->next->next) {
      node->val = node->next->val;
      node = node->next;
    }
    if (node) {
      node->val = node->next->val;
      node->next = nullptr;
    }
  }
};
