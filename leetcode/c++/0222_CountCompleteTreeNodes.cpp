struct TreeNode {
  int val;
  TreeNode *left;
  TreeNode *right;
  TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
};

class Solution {
 public:
  int countNodes(TreeNode *root) { return countNodes(root, -1, -1); }

 private:
  int countNodes(TreeNode *root, int left_depth, int right_depth) {
    if (root == nullptr) {
      return 0;
    }
    if (left_depth == -1) {
      left_depth = 1;
      for (TreeNode *left = root->left; left != nullptr; left = left->left) {
        ++left_depth;
      }
    }
    if (right_depth == -1) {
      right_depth = 1;
      for (TreeNode *right = root->right; right != nullptr; right = right->right) {
        ++right_depth;
      }
    }
    if (left_depth == right_depth) {
      // Perfect
      return (1 << left_depth) - 1;
    }
    return 1 + countNodes(root->left, left_depth - 1, -1) +
           countNodes(root->right, -1, right_depth - 1);
  }
};
