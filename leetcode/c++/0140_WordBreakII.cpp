#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>
using namespace std;

class Solution {
 public:
  vector<string> wordBreak(string s, vector<string>& wordDict) {
    const unordered_set<string> words(wordDict.begin(), wordDict.end());
    unordered_map<string, vector<string>> cache;  // result of (i]
    int max_len = 0;
    for (const string& word : words) {
      max_len = std::max(max_len, int(word.length()));
    }
    return wordBreak(s, words, max_len, &cache);
  }

 private:
  const vector<string>& wordBreak(const string& s, const unordered_set<string>& wordDict,
                                  const int max_len, unordered_map<string, vector<string>>* cache) {
    if (cache->find(s) != cache->end()) {
      return cache->at(s);
    }

    vector<string>& result = cache->emplace(s, vector<string>()).first->second;
    const int limit = std::min<int>(s.length(), max_len);
    for (int len = 1; len <= limit; ++len) {
      const string word = s.substr(0, len);
      if (wordDict.find(word) == wordDict.end()) {
        continue;
      }
      if (len == s.length()) {
        result.push_back(word);
      } else {
        const vector<string>& postfix = wordBreak(s.substr(len), wordDict, max_len, cache);
        for (const string& candi : postfix) {
          result.push_back(word + " " + candi);
        }
      }
    }
    return result;
  }
};
