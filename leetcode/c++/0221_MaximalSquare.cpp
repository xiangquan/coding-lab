#include <algorithm>
#include <vector>
using namespace std;

class Solution {
 public:
  int maximalSquare(vector<vector<char>>& matrix) {
    if (matrix.empty()) {
      return 0;
    }
    vector<vector<int>> continuousOnes(matrix.size(), vector<int>(matrix[0].size(), 0));
    int maxLen = 0;
    for (int i = 0; i < matrix.size(); ++i) {
      int currentContinuousOnes = 0;
      for (int j = 0; j < matrix[0].size(); ++j) {
        if (matrix[i][j] == '0') {
          currentContinuousOnes = 0;
          continue;
        }
        maxLen = std::max(maxLen, 1);

        continuousOnes[i][j] = ++currentContinuousOnes;
        for (int len = maxLen + 1; len <= std::min(i + 1, j + 1); ++len) {
          if (isSquare(continuousOnes, i, j, len)) {
            maxLen = len;
          } else {
            break;
          }
        }
      }
    }
    return maxLen * maxLen;
  }

 private:
  bool isSquare(const vector<vector<int>>& continuousOnes, int i, int j, int len) {
    for (int k = 0; k < len; ++k) {
      if (continuousOnes[i - k][j] < len) {
        return false;
      }
    }
    return true;
  }
};
