#include <unordered_map>
using namespace std;

// Definition for singly-linked list with a random pointer.
struct RandomListNode {
  int label;
  RandomListNode *next, *random;
  RandomListNode(int x) : label(x), next(nullptr), random(nullptr) {}
};

class Solution {
 public:
  RandomListNode *copyRandomList(RandomListNode *head) {
    if (!head) {
      return nullptr;
    }

    unordered_map<RandomListNode *, RandomListNode *> mapping;
    return copyRandomList(head, mapping);
  }

  RandomListNode *copyRandomList(RandomListNode *head,
                                 unordered_map<RandomListNode *, RandomListNode *> &mapping) {
    if (mapping.find(head) != mapping.end()) {
      return mapping[head];
    }

    RandomListNode *new_head = new RandomListNode(head->label);
    mapping.emplace(head, new_head);
    if (head->next) {
      new_head->next = copyRandomList(head->next, mapping);
    }
    if (head->random) {
      new_head->random = copyRandomList(head->random, mapping);
    }
    return new_head;
  }
};
