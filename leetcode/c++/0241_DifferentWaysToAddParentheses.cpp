#include <cstdlib>
#include <string>
#include <vector>
using namespace std;

class Solution {
 public:
  vector<int> diffWaysToCompute(string input) {
    return diffWaysToCompute(input, 0, input.length());
  }

 private:
  vector<int> diffWaysToCompute(const string& input, int start, int end) {
    vector<int> result;
    for (int i = start + 1; i + 1 < end; ++i) {
      if (input[i] != '+' && input[i] != '-' && input[i] != '*') {
        continue;
      }
      const vector<int> left = diffWaysToCompute(input, start, i);
      const vector<int> right = diffWaysToCompute(input, i + 1, end);
      for (const int l : left) {
        for (const int r : right) {
          if (input[i] == '+') {
            result.push_back(l + r);
          } else if (input[i] == '-') {
            result.push_back(l - r);
          } else {
            result.push_back(l * r);
          }
        }
      }
    }
    if (result.empty()) {
      result.push_back(std::atoi(input.c_str() + start));
    }
    return result;
  }
};
