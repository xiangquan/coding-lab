struct TreeNode {
  int val;
  TreeNode *left;
  TreeNode *right;
  TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
};

class Solution {
 public:
  int kthSmallest(TreeNode *root, int k) {
    int n;
    return kthSmallestOrTotalNodes(root, k, &n);
  }

 private:
  int kthSmallestOrTotalNodes(TreeNode *root, int k, int *n) {
    int left_n = 0, right_n = 0;
    if (root->left != nullptr) {
      const int kth = kthSmallestOrTotalNodes(root->left, k, &left_n);
      // Found in left tree.
      if (left_n < 0) {
        *n = -1;
        return kth;
      }
    }
    // Fount at current node.
    if (left_n == k - 1) {
      *n = -1;
      return root->val;
    }

    if (root->right != nullptr) {
      const int kth = kthSmallestOrTotalNodes(root->right, k - left_n - 1, &right_n);
      // Found in left tree.
      if (right_n < 0) {
        *n = -1;
        return kth;
      }
    }
    *n = left_n + right_n + 1;
    return -1;
  }
};
