#include <memory>
#include <unordered_map>
#include <vector>
using namespace std;

class Solution {
 public:
  vector<int> findOrder(int numCourses, vector<vector<int>>& prerequisites) {
    unordered_map<int, unique_ptr<Node>> graph;
    for (const auto& edge : prerequisites) {
      const int dependency = edge[0];
      const int prerequisite = edge[1];
      if (graph.find(prerequisite) == graph.end()) {
        graph.emplace(prerequisite, new Node(prerequisite));
      }
      if (graph.find(dependency) == graph.end()) {
        graph.emplace(dependency, new Node(dependency));
      }
      graph[prerequisite]->addDependency(graph[dependency].get());
    }

    vector<int> result;
    for (int i = 0; i < numCourses; ++i) {
      if (graph.find(i) == graph.end()) {
        result.push_back(i);
      }
    }
    for (auto& node : graph) {
      if (node.second->prerequisites == 0) {
        node.second->study(&result);
      }
    }
    if (result.size() != numCourses) {
      result.clear();
    }
    return result;
  }

 private:
  struct Node {
    int course_id;
    int prerequisites = 0;
    vector<Node*> dependencies;

    Node(int id) : course_id(id) {}

    void addDependency(Node* node) {
      ++(node->prerequisites);
      dependencies.push_back(node);
    }

    void finishOnePrerequisite(vector<int>* result) {
      if (--prerequisites == 0) {
        study(result);
      }
    }

    void study(vector<int>* result) {
      result->push_back(course_id);
      for (Node* node : dependencies) {
        node->finishOnePrerequisite(result);
      }
      prerequisites = -1;
    }
  };
};
