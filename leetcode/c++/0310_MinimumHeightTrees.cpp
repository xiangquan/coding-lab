#include <unordered_map>
#include <unordered_set>
#include <vector>

using namespace std;

class Solution {
 public:
  vector<int> findMinHeightTrees(int n, vector<vector<int>>& edges) {
    unordered_map<int, unordered_set<int>> neighbors;
    for (int i = 0; i < n; ++i) {
      neighbors[i] = {};
    }
    for (const auto& edge : edges) {
      neighbors[edge[0]].insert(edge[1]);
      neighbors[edge[1]].insert(edge[0]);
    }

    vector<int> result;

    // Initial step.
    unordered_set<int> step;
    for (const auto& item : neighbors) {
      if (item.second.size() == 0) {
        if (n == 1) {
          result.push_back(0);
        }
        return result;
      }
      if (item.second.size() == 1) {
        step.insert(item.first);
      }
    }

    // Walk towards center.
    while (true) {
      unordered_set<int> next_step;
      for (const int from : step) {
        for (const int to : neighbors[from]) {
          neighbors[to].erase(from);
          if (neighbors[to].size() == 1) {
            next_step.insert(to);
          }
        }
      }
      if (next_step.empty()) {
        break;
      }
      next_step.swap(step);
    }

    result.assign(step.begin(), step.end());
    return result;
  }
};
