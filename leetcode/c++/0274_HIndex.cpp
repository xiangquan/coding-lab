#include <algorithm>
#include <vector>
using namespace std;

class Solution {
 public:
  int hIndex(vector<int>& citations) {
    if (citations.empty()) {
      return 0;
    }
    std::make_heap(citations.begin(), citations.end(), std::greater<int>());
    while (!citations.empty()) {
      if (citations[0] >= citations.size()) {
        return citations.size();
      }
      std::pop_heap(citations.begin(), citations.end(), std::greater<int>());
      citations.pop_back();
    }
    return 0;
  }
};
