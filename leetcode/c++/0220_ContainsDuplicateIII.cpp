#include <set>
#include <vector>
using namespace std;

class Solution {
 public:
  bool containsNearbyAlmostDuplicate(vector<int>& nums, int k, int t) {
    if (nums.empty() || k <= 0 || t < 0) {
      return false;
    }
    multiset<long> range;
    int i = 0;
    range.insert(nums[0]);
    for (int j = 1; j < nums.size(); ++j) {
      if (j - i > k) {
        range.erase(range.lower_bound(nums[i++]));
      }
      long val = nums[j];
      auto lower_bound = range.lower_bound(val - t);
      auto upper_bound = range.upper_bound(val + t);
      if (lower_bound != upper_bound) {
        return true;
      }
      range.insert(nums[j]);
    }
    return false;
  }
};
