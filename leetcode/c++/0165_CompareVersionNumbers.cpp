#include <string>
#include <vector>
using namespace std;

class Solution {
 public:
  int compareVersion(string version1, string version2) {
    const auto ver1 = convert(version1);
    const auto ver2 = convert(version2);
    int i = 0;
    while (i < ver1.size() && i < ver2.size()) {
      if (ver1[i] < ver2[i]) {
        return -1;
      }
      if (ver1[i] > ver2[i]) {
        return 1;
      }
      ++i;
    }

    while (i < ver1.size()) {
      if (ver1[i] > 0) {
        return 1;
      }
      ++i;
    }
    while (i < ver2.size()) {
      if (ver2[i] > 0) {
        return -1;
      }
      ++i;
    }
    return 0;
  }

  vector<int> convert(const string& ver) {
    vector<int> ret;

    int l = 0;
    while (l < ver.length()) {
      int r = l + 1;
      while (r < ver.length() && ver[r] != '.') {
        ++r;
      }
      ret.push_back(atoi(ver.substr(l, r - l).c_str()));
      l = r + 1;
    }
    return ret;
  }
};
