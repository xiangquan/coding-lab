#include <stack>
using namespace std;

// Definition for binary tree
struct TreeNode {
  int val;
  TreeNode *left;
  TreeNode *right;
  TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
};

class SimpleSolution {
 public:
  void flatten(TreeNode *root) {
    if (root == nullptr) {
      return;
    } else if (root->left == nullptr) {
      flatten(root->right);
    } else {
      TreeNode *left = root->left;
      TreeNode *right = root->right;
      flatten(left);
      flatten(right);
      root->left = nullptr;
      root->right = left;
      while (root->right != nullptr) {
        root = root->right;
      }
      root->right = right;
    }
  }
};

class Solution {
 public:
  void flatten(TreeNode *root) {
    stack<TreeNode *> rights;
    while (root != nullptr) {
      if (root->left == nullptr) {
        if (root->right == nullptr) {
          if (rights.empty()) {
            break;
          }
          root->right = rights.top();
          rights.pop();
        }
      } else {
        if (root->right) {
          rights.push(root->right);
        }
        root->right = root->left;
        root->left = nullptr;
      }
      root = root->right;
    }
  }
};
