class NumMatrix {
 public:
  NumMatrix(vector<vector<int>>& matrix) : cached_sum_(matrix) {
    for (int i = 0; i < matrix.size(); ++i) {
      for (int j = 0; j < matrix[0].size(); ++j) {
        if (i > 0) {
          cached_sum_[i][j] += cached_sum_[i - 1][j];
        }
        if (j > 0) {
          cached_sum_[i][j] += cached_sum_[i][j - 1];
        }
        if (i > 0 && j > 0) {
          cached_sum_[i][j] -= cached_sum_[i - 1][j - 1];
        }
      }
    }
  }

  int sumRegion(int row1, int col1, int row2, int col2) {
    int sum = cached_sum_[row2][col2];
    if (row1 > 0) {
      sum -= cached_sum_[row1 - 1][col2];
    }
    if (col1 > 0) {
      sum -= cached_sum_[row2][col1 - 1];
    }
    if (row1 > 0 && col1 > 0) {
      sum += cached_sum_[row1 - 1][col1 - 1];
    }
    return sum;
  }

 private:
  vector<vector<int>> cached_sum_;
};
