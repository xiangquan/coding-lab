#include <string>
#include <vector>
using namespace std;

// Definition for a binary tree node.
struct TreeNode {
  int val;
  TreeNode* left;
  TreeNode* right;
  TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
};

class Solution {
 public:
  vector<string> binaryTreePaths(TreeNode* root) {
    vector<string> result;
    if (!root) {
      return result;
    }

    binaryTreePaths(result, "", root);
    return result;
  }

  void binaryTreePaths(vector<string>& result, const string& prefix, TreeNode* root) {
    const string next = prefix.empty() ? itoa(root->val) : prefix + "->" + itoa(root->val);
    if (!root->left && !root->right) {
      result.push_back(next);
      return;
    }

    if (root->left) {
      binaryTreePaths(result, next, root->left);
    }
    if (root->right) {
      binaryTreePaths(result, next, root->right);
    }
  }

  string itoa(int i) {
    char buf[16];
    sprintf(buf, "%d", i);
    return buf;
  }
};
