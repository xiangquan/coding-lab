struct ListNode {
  int val;
  ListNode* next;
  ListNode(int x) : val(x), next(nullptr) {}
};

class Solution {
 public:
  ListNode* getIntersectionNode(ListNode* headA, ListNode* headB) {
    ListNode* tailA = headA;
    ListNode* tailB = headB;
    while (tailA != nullptr && tailA->next != nullptr) {
      tailA = tailA->next;
    }
    while (tailB != nullptr && tailB->next != nullptr) {
      tailB = tailB->next;
    }
    if (tailA != tailB) {
      return nullptr;
    }

    const int lenA = lenOfList(headA);
    const int lenB = lenOfList(headB);

    ListNode* reversedB = reverseList(headB);
    const int newLenA = lenOfList(headA);
    reverseList(reversedB);
    const int distToTail = (lenA + lenB - newLenA + 1) / 2;
    const int distToHead = lenA - distToTail;
    for (int i = 0; i < distToHead; ++i) {
      headA = headA->next;
    }
    return headA;
  }

 private:
  int lenOfList(ListNode* head) {
    int len = 0;
    while (head != nullptr) {
      ++len;
      head = head->next;
    }
    return len;
  }

  ListNode* reverseList(ListNode* head) {
    ListNode* cur = nullptr;
    ListNode* next = head;
    while (next != nullptr) {
      ListNode* tmp = next->next;
      next->next = cur;
      cur = next;
      next = tmp;
    }
    return cur;
  }
};