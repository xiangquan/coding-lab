#include <algorithm>
#include <vector>
using namespace std;

class Solution {
 public:
  vector<int> largestDivisibleSubset(vector<int>& nums) {
    if (nums.empty()) {
      return {};
    }

    sort(nums.begin(), nums.end());

    vector<int> ret(nums.size(), 0);
    int ret_count = 0, index = 0;
    for (int i = 0; i < nums.size(); ++i) {
      int largest = 0;
      for (int j = i - 1; j >= 0; --j) {
        if (ret[j] > largest && nums[i] % nums[j] == 0) {
          largest = ret[j];
        }
      }
      ret[i] = largest + 1;
      if (ret_count < ret[i]) {
        ret_count = ret[i];
        index = i;
      }
    }
    vector<int> result(1, nums[index]);
    --ret_count;
    for (int i = index - 1; i >= 0; --i) {
      if (nums[index] % nums[i] == 0 && ret[i] == ret_count) {
        result.push_back(nums[i]);
        index = i;
        --ret_count;
      }
    }

    return result;
  }
};
