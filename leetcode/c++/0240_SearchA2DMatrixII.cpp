#include <vector>
using namespace std;

class Solution {
 public:
  bool searchMatrix(vector<vector<int>>& matrix, int target) {
    if (matrix.empty() || matrix[0].empty()) {
      return false;
    }
    return searchMatrix(matrix, 0, 0, matrix.size(), matrix[0].size(), target);
  }

 public:
  // Search [left, right)
  bool searchMatrix(const vector<vector<int>>& matrix, const int left_i, const int left_j,
                    const int right_i, const int right_j, const int target) {
    if (left_i == right_i || left_j == right_j) {
      return false;
    }
    if (left_i + 2 >= right_i && left_j + 2 >= right_j) {
      for (int i = left_i; i < right_i; ++i) {
        for (int j = left_j; j < right_j; ++j) {
          if (matrix[i][j] == target) {
            return true;
          }
        }
      }
      return false;
    }

    const int mid_i = (left_i + right_i) / 2;
    const int mid_j = (left_j + right_j) / 2;
    if (searchMatrix(matrix, left_i, mid_j + 1, mid_i, right_j, target) ||
        searchMatrix(matrix, mid_i + 1, left_j, right_i, mid_j, target)) {
      return true;
    }
    return target <= matrix[mid_i][mid_j]
               ? searchMatrix(matrix, left_i, left_j, mid_i + 1, mid_j + 1, target)
               : searchMatrix(matrix, mid_i, mid_j, right_i, right_j, target);
  }
};
