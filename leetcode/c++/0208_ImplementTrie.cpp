#include <memory>
#include <string>
#include <unordered_map>
using namespace std;

class Trie {
 public:
  Trie() {}

  void insert(string word) { root_.insert(word, 0); }

  bool search(string word) { return root_.search(word, 0); }

  bool startsWith(string prefix) { return root_.startsWith(prefix, 0); }

 private:
  struct Node {
    bool ending = false;
    unordered_map<char, std::unique_ptr<Node>> children;

    void insert(const string& word, const int index) {
      if (index == word.length()) {
        ending = true;
        return;
      }
      const char c = word[index];
      if (children.find(c) == children.end()) {
        children.emplace(c, new Node);
      }
      children[c]->insert(word, index + 1);
    }

    bool search(const string& word, const int index) {
      if (index == word.length()) {
        return ending;
      }
      const char c = word[index];
      if (children.find(c) == children.end()) {
        return false;
      }
      return children[c]->search(word, index + 1);
    }

    bool startsWith(const string& prefix, const int index) {
      if (index == prefix.length()) {
        return true;
      }
      const char c = prefix[index];
      if (children.find(c) == children.end()) {
        return false;
      }
      return children[c]->startsWith(prefix, index + 1);
    }
  };

  Node root_;
};
