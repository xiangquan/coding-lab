#include <memory>
#include <vector>
using namespace std;

class Solution {
 public:
  vector<int> countSmaller(vector<int>& nums) {
    Node root;
    vector<int> result(nums.size(), 0);
    for (int i = nums.size(); i > 0; --i) {
      result[i - 1] = root.add(nums[i - 1]);
    }
    return result;
  }

 private:
  struct Node {
    int value;
    int count = 0;
    std::unique_ptr<Node> left = nullptr;
    int left_count = 0;
    std::unique_ptr<Node> right = nullptr;
    int right_count = 0;

    int add(const int num) {
      if (count == 0 || num == value) {
        value = num;
        ++count;
        return left_count;
      } else if (num < value) {
        ++left_count;
        if (left == nullptr) {
          left.reset(new Node);
        }
        return left->add(num);
      } else {
        ++right_count;
        if (right == nullptr) {
          right.reset(new Node);
        }
        return left_count + count + right->add(num);
      }
    }
  };
};
