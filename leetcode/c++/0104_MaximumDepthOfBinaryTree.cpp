#include <queue>
#include <tuple>
using namespace std;

// Definition for binary tree
struct TreeNode {
  int val;
  TreeNode *left;
  TreeNode *right;
  TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
};

class SimpleSolution {
 public:
  int maxDepth(TreeNode *root) {
    if (root == nullptr) {
      return 0;
    }
    return std::max(maxDepth(root->left), maxDepth(root->right)) + 1;
  }
};

class Solution {
 public:
  int maxDepth(TreeNode *root) {
    int result = 0;
    queue<tuple<TreeNode *, int>> todo;
    todo.emplace(root, 0);

    while (!todo.empty()) {
      TreeNode *node;
      int depth;
      std::tie(node, depth) = todo.front();
      if (node == nullptr) {
        result = std::max(result, depth);
      } else {
        todo.emplace(node->left, depth + 1);
        todo.emplace(node->right, depth + 1);
      }
      todo.pop();
    }

    return result;
  }
};
