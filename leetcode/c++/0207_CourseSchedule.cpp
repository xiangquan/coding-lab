#include <memory>
#include <unordered_map>
#include <vector>
using namespace std;

class Solution {
 public:
  bool canFinish(int numCourses, vector<vector<int>>& prerequisites) {
    unordered_map<int, unique_ptr<Node>> graph;
    for (const auto& edge : prerequisites) {
      const int dependency = edge[0];
      const int prerequisite = edge[1];
      if (graph.find(prerequisite) == graph.end()) {
        graph.emplace(prerequisite, new Node);
      }
      if (graph.find(dependency) == graph.end()) {
        graph.emplace(dependency, new Node);
      }
      graph[prerequisite]->addDependency(graph[dependency].get());
    }

    for (auto& node : graph) {
      if (node.second->prerequisites == 0) {
        node.second->study();
      }
    }
    for (auto& node : graph) {
      if (node.second->prerequisites != -1) {
        return false;
      }
    }
    return true;
  }

 private:
  struct Node {
    int prerequisites = 0;
    vector<Node*> dependencies;

    void addDependency(Node* node) {
      ++(node->prerequisites);
      dependencies.push_back(node);
    }

    void finishOnePrerequisite() {
      if (--prerequisites == 0) {
        study();
      }
    }

    void study() {
      for (Node* node : dependencies) {
        node->finishOnePrerequisite();
      }
      prerequisites = -1;
    }
  };
};
