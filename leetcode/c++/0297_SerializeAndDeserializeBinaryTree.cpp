#include <string>
using namespace std;

struct TreeNode {
  int val;
  TreeNode* left;
  TreeNode* right;
  TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
};

class Codec {
 public:
  string serialize(TreeNode* root) {
    if (root == nullptr) {
      return "";
    }
    string val = std::to_string(root->val);
    if (root->left == nullptr && root->right == nullptr) {
      return val;
    }
    const string left = serialize(root->left);
    return val + kDelimiter + std::to_string(left.length()) + kDelimiter + left +
           serialize(root->right);
  }

  TreeNode* deserialize(string data) { return deserialize(data, 0, data.length()); }

 private:
  TreeNode* deserialize(const string& data, const size_t start, const size_t end) {
    if (start == end) {
      return nullptr;
    }
    size_t first_deli = start + 1;
    for (; first_deli < end; ++first_deli) {
      if (data[first_deli] == ',') {
        break;
      }
    }
    const string val = data.substr(start, first_deli - start);
    auto* node = new TreeNode(std::atoi(val.c_str()));
    if (first_deli == end) {
      return node;
    }
    const size_t second_deli = data.find_first_of(kDelimiter, first_deli + 1);
    const size_t left_len = std::atoi(data.c_str() + first_deli + 1);
    const size_t left_end = second_deli + 1 + left_len;

    node->left = deserialize(data, second_deli + 1, left_end);
    node->right = deserialize(data, left_end, end);
    return node;
  }

  const char* kDelimiter = ",";
};
