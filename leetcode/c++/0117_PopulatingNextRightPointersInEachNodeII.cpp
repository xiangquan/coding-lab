// Definition for binary tree with next pointer.
struct Node {
  int val;
  Node *left, *right, *next;
  Node(int x) : val(x), left(nullptr), right(nullptr), next(nullptr) {}
};

class SimpleSolution {
 public:
  Node* connect(Node* root) {
    if (root == nullptr) {
      return root;
    }
    Node* left = root->left;
    Node* right = root->right;
    connect(left);
    connect(right);
    while (left && right) {
      Node* next_left = most_left_son(left);
      Node* next_right = most_left_son(right);
      most_right_sibling(left)->next = right;
      left = next_left;
      right = next_right;
    }
    return root;
  }

 private:
  Node* most_left_son(Node* node) {
    while (node) {
      if (node->left) {
        return node->left;
      } else if (node->right) {
        return node->right;
      }
      node = node->next;
    }
    return nullptr;
  }

  Node* most_right_sibling(Node* node) {
    while (node->next) {
      node = node->next;
    }
    return node;
  }
};

class Solution {
 public:
  Node* connect(Node* root) {
    Node* node = root;
    while (node) {
      Node* nextLevel = nullptr;

      // connect one level
      while (node) {
        // assign the next level head
        if (nextLevel == nullptr) {
          nextLevel = node->left ? node->left : node->right;
        }

        // connect my children
        if (node->left && node->right) {
          node->left->next = node->right;
        }
        // find my rightest child
        Node* myRightestChild = node->right ? node->right : node->left;
        // find the brother of my rightest child
        if (myRightestChild) {
          Node* myRightestChildNext = nullptr;

          Node* brother = node->next;
          while (brother) {
            if (brother->left) {
              myRightestChildNext = brother->left;
              break;
            } else if (brother->right) {
              myRightestChildNext = brother->right;
              break;
            }
            brother = brother->next;
          }
          myRightestChild->next = myRightestChildNext;
        }

        node = node->next;
      }

      node = nextLevel;
    }
    return root;
  }
};
