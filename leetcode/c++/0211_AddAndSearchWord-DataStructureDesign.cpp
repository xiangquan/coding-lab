#include <string>
using namespace std;

class WordDictionary {
 public:
  WordDictionary() {}

  void addWord(string word) { root_.addWord(word, 0); }

  bool search(string word) { return root_.search(word, 0); }

 private:
  struct Node {
    bool ending = false;
    unordered_map<char, std::unique_ptr<Node>> children;

    void addWord(const string& word, const int index) {
      if (index == word.length()) {
        ending = true;
        return;
      }
      const char c = word[index];
      if (children.find(c) == children.end()) {
        children.emplace(c, new Node);
      }
      children[c]->addWord(word, index + 1);
    }

    bool search(const string& word, const int index) {
      if (index == word.length()) {
        return ending;
      }
      const char c = word[index];
      if (c != '.') {
        if (children.find(c) == children.end()) {
          return false;
        }
        return children[c]->search(word, index + 1);
      }
      for (const auto& child : children) {
        if (child.second->search(word, index + 1)) {
          return true;
        }
      }
      return false;
    }
  };

  Node root_;
};
