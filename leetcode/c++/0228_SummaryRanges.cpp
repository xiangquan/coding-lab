#include <string>
#include <vector>
using namespace std;

class Solution {
 public:
  vector<string> summaryRanges(vector<int>& nums) {
    vector<string> result;
    if (nums.empty()) {
      return result;
    }
    int left = nums[0];
    for (int i = 1; i < nums.size(); ++i) {
      if (nums[i] == nums[i - 1] + 1) {
        continue;
      }
      int right = nums[i - 1];
      if (left == right) {
        result.push_back(std::to_string(left));
      } else {
        result.push_back(std::to_string(left) + "->" + std::to_string(right));
      }
      left = nums[i];
    }
    int right = nums.back();
    if (left == right) {
      result.push_back(std::to_string(left));
    } else {
      result.push_back(std::to_string(left) + "->" + std::to_string(right));
    }
    return result;
  }
};
