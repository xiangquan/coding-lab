#include <vector>
using namespace std;

class Solution {
 public:
  int superPow(int a, vector<int>& b) {
    vector<int> pow_of_a(1, 1);  // a^0, a^1, ... a^9
    pow_of_a.push_back(a % 1337);
    for (int i = 2; i <= 9; ++i) {
      pow_of_a.push_back((pow_of_a.back() * pow_of_a[1]) % 1337);
    }

    int res = 1;
    for (int bit : b) {
      const int res_pow_2 = (res * res) % 1337;
      const int res_pow_4 = (res_pow_2 * res_pow_2) % 1337;
      const int res_pow_8 = (res_pow_4 * res_pow_4) % 1337;
      // res = ((res^10) * a^bit) % 1337
      //     = res_pow_8 * res_pow_2
      res = (((res_pow_8 * res_pow_2) % 1337) * pow_of_a[bit]) % 1337;
    }
    return res;
  }
};
