#include <unordered_map>
using namespace std;

class LRUCache {
 public:
  LRUCache(int capacity) { cap = capacity; }

  int get(int key) {
    if (cache.find(key) != cache.end()) {
      Item* item = cache[key];
      if (item != head) {
        item->prev->next = item->next;
        if (item->next) {
          item->next->prev = item->prev;
        }
        item->next = head;
        head->prev = item;
        item->prev = nullptr;
        head = item;
      }
      return head->val;
    }
    return -1;
  }

  void set(int key, int value) {
    if (cap == 0) {
      return;
    }

    Item* item;
    if (cache.find(key) != cache.end()) {
      item = cache[key];
      if (item->prev) {
        item->prev->next = item->next;
        if (item->next) {
          item->next->prev = item->prev;
        }
      }
    } else if (cache.size() < cap) {
      item = new Item;
      cache.emplace(key, item);
    } else {
      item = head;
      while (item->next) {
        item = item->next;
      }
      if (item->prev) {
        item->prev->next = nullptr;
      }
      cache.erase(item->key);
      cache.emplace(key, item);
    }
    item->key = key;
    item->val = value;
    item->prev = nullptr;
    if (head != item) {
      if (head) {
        head->prev = item;
      }
      item->next = head;
    }
    head = item;
  }

 private:
  struct Item {
    Item* prev = nullptr;
    Item* next = nullptr;
    int key;
    int val;
  };

  unordered_map<int, Item*> cache;
  Item* head = nullptr;
  int cap;
};
