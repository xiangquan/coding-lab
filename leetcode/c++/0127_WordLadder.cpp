#include <queue>
#include <set>
#include <string>
#include <unordered_set>
using namespace std;

class Solution {
 public:
  int ladderLength(string beginWord, string endWord, vector<string>& wordList) {
    if (beginWord == endWord) {
      return 0;
    }
    unordered_set<string> dict(wordList.begin(), wordList.end());

    int steps = 2;
    const string empty;
    queue<string> processingQueue;
    set<string> visited;
    processingQueue.push(beginWord);
    processingQueue.push(empty);
    visited.insert(beginWord);

    while (true) {
      string& word = processingQueue.front();
      // finish one step
      if (word.empty()) {
        ++steps;
        if (processingQueue.size() == 1) {
          break;
        }
        processingQueue.push(empty);
      } else {
        // generate every word of one edit distance
        for (int i = word.length() - 1; i >= 0; --i) {
          string oneEditDistanceWord(word);
          for (char c = 'a'; c <= 'z'; ++c) {
            oneEditDistanceWord[i] = c;
            // not in dict or has been visited
            if (dict.find(oneEditDistanceWord) == dict.end() ||
                visited.find(oneEditDistanceWord) != visited.end()) {
              continue;
            }

            // reach the end
            if (oneEditDistanceWord == endWord) {
              return steps;
            }

            processingQueue.push(oneEditDistanceWord);
            visited.insert(oneEditDistanceWord);
          }
        }
      }
      processingQueue.pop();
    }
    return 0;
  }
};
