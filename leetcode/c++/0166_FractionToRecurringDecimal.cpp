#include <string>
#include <unordered_map>
using namespace std;

class Solution {
 public:
  string fractionToDecimal(int numerator, int denominator) {
    if (numerator == 0) {
      return "0";
    }
    bool neg = (numerator < 0) ^ (denominator < 0);
    long numerator64 = numerator;
    if (numerator64 < 0) {
      numerator64 = -numerator64;
    }
    long denominator64 = denominator;
    if (denominator64 < 0) {
      denominator64 = -denominator64;
    }

    string result = (neg ? "-" : "") + std::to_string(numerator64 / denominator64);
    return numerator64 % denominator64 == 0
               ? result
               : result + "." + decimalPart(numerator64 % denominator64, denominator64);
  }

 private:
  string decimalPart(long numerator, long denominator) {
    unordered_map<long, std::size_t> numerator_pos;
    string result;
    while (numerator != 0) {
      numerator *= 10;
      if (numerator_pos.find(numerator) == numerator_pos.end()) {
        numerator_pos[numerator] = result.length();
        result += std::to_string(numerator / denominator);
        numerator %= denominator;
      } else {
        result.insert(numerator_pos[numerator], "(");
        result += ')';
        break;
      }
    }
    return result;
  }
};
