#include <unordered_set>
#include <vector>
using namespace std;

class Solution {
 public:
  int longestConsecutive(vector<int> &nums) {
    if (nums.empty()) {
      return 0;
    }
    // use hash-based set, not tree-based set
    unordered_set<int> showup;

    for (const int num : nums) {
      showup.insert(num);
    }

    int maxLen = 1, curLen;
    for (const int num : nums) {
      if (showup.find(num) == showup.end()) {
        continue;
      }

      curLen = 1;
      for (int i = num - 1; showup.find(i) != showup.end(); --i) {
        ++curLen;
        showup.erase(i);
      }
      for (int i = num + 1; showup.find(i) != showup.end(); ++i) {
        ++curLen;
        showup.erase(i);
      }
      maxLen = std::max(maxLen, curLen);
    }
    return maxLen;
  }
};
