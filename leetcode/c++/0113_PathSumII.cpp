#include <vector>
using namespace std;

// Definition for binary tree
struct TreeNode {
  int val;
  TreeNode *left;
  TreeNode *right;
  TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
};

class Solution {
 public:
  vector<vector<int> > pathSum(TreeNode *root, int sum) {
    vector<int> prefix;
    vector<vector<int> > result;
    pathSum(root, sum, prefix, result);
    return result;
  }

 private:
  void pathSum(TreeNode *root, int sum, vector<int> &prefix, vector<vector<int> > &result) {
    if (root == nullptr) {
      return;
    }

    sum -= root->val;
    prefix.push_back(root->val);

    if (sum == 0 && root->left == nullptr && root->right == nullptr) {
      result.push_back(prefix);
    } else {
      pathSum(root->left, sum, prefix, result);
      pathSum(root->right, sum, prefix, result);
    }

    prefix.pop_back();
  }
};
