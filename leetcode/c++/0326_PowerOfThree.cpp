class Solution {
 public:
  bool isPowerOfThree(int n) {
    if (n <= 0) {
      return false;
    }

    int p_of_3 = 1;
    while (true) {
      if (p_of_3 == n) {
        return true;
      }

      if (n / p_of_3 < 3) {
        return false;
      }
      p_of_3 *= 3;
    }

    return false;
  }
};
