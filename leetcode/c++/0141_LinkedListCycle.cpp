struct ListNode {
  int val;
  ListNode* next;
  ListNode(int x) : val(x), next(nullptr) {}
};

class Solution {
 public:
  bool hasCycle(ListNode* head) {
    if (head == nullptr) {
      return false;
    }
    ListNode* walker1 = head;
    ListNode* walker2 = head->next;
    while (walker2 != nullptr && walker1 != walker2) {
      walker1 = walker1->next;
      if (walker2->next == nullptr) {
        return false;
      }
      walker2 = walker2->next->next;
    }
    return walker1 == walker2;
  }
};
