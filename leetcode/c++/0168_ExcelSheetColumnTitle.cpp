#include <string>
using namespace std;

class Solution {
 public:
  string convertToTitle(int n) {
    string ret;
    while (n) {
      const char bit = 'A' + ((n - 1) % 26);
      ret = bit + ret;
      n = (n - 1) / 26;
    }
    return ret;
  }
};
