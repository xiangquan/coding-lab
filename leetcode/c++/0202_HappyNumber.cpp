#include <unordered_set>
using namespace std;

class Solution {
 public:
  bool isHappy(int n) {
    unordered_set<int> met;

    while (met.find(n) == met.end()) {
      met.insert(n);

      int square_sum = 0;
      while (n) {
        int tmp = (n % 10);
        square_sum += tmp * tmp;
        n /= 10;
      }
      n = square_sum;
    }
    return n == 1;
  }
};
