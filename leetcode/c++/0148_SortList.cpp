struct ListNode {
  int val;
  ListNode* next;
  ListNode(int x) : val(x), next(nullptr) {}
};

class Solution {
 public:
  ListNode* sortList(ListNode* head) {
    int n = 0;
    for (ListNode* tail = head; tail != nullptr; tail = tail->next) {
      ++n;
    }
    return n > 1 ? sortList(head, n) : head;
  }

 private:
  ListNode* sortList(ListNode* head, int n) {
    if (n == 1) {
      head->next = nullptr;
      return head;
    }
    if (n == 2) {
      if (head->val <= head->next->val) {
        head->next->next = nullptr;
        return head;
      }
      ListNode* next = head->next;
      next->next = head;
      head->next = nullptr;
      return next;
    }
    const int left = n / 2;
    ListNode* right_head = head;
    for (int i = 0; i < left; ++i) {
      right_head = right_head->next;
    }
    return mergeLists(sortList(head, left), sortList(right_head, n - left));
  }

  ListNode* mergeLists(ListNode* left, ListNode* right) {
    ListNode* head = nullptr;
    ListNode* tail = nullptr;
    while (left != nullptr && right != nullptr) {
      ListNode* next;
      if (left->val < right->val) {
        next = left;
        left = left->next;
      } else {
        next = right;
        right = right->next;
      }
      if (tail != nullptr) {
        tail->next = next;
      } else {
        head = next;
      }
      tail = next;
    }
    if (left != nullptr) {
      tail->next = left;
    } else {
      tail->next = right;
    }
    return head;
  }
};