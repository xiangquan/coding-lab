// Definition for singly-linked list.
struct ListNode {
  int val;
  ListNode* next;
  ListNode(int x) : val(x), next(nullptr) {}
};

class Solution {
 public:
  ListNode* removeElements(ListNode* head, int val) {
    ListNode* pre = nullptr;
    ListNode* cur = head;

    while (cur) {
      if (cur->val != val) {
        if (!pre) {
          head = cur;
        } else {
          pre->next = cur;
        }
        pre = cur;
      }
      cur = cur->next;
    }
    if (pre) {
      pre->next = nullptr;
    }
    return pre ? head : nullptr;
  }
};
