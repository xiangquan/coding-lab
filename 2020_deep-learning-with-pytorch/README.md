# [Deep Learning with PyTorch](https://book.douban.com/subject/34701117/)

## Environment

* Ubuntu 20.04
* Bazel 3.2
* Python 3.8
  * [Python requirements](tools/requirements.txt)
