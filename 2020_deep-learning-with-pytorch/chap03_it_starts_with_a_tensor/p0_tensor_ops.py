#!/usr/bin/env python

import h5py
import numpy as np
import torch


# Create with ones, zeros, list, random.
ones = torch.ones(3)  # tensor([1., 1., 1.])
zeros = torch.zeros(2, 3)  # tensor([[0., 0., 0.], [0., 0., 0.]])
print(zeros.shape)  # torch.Size([2, 3])
print(torch.tensor([1, 2, 3]))
print(torch.randn(3, 5, 5))

# From/to numpy.
print(torch.from_numpy(np.array([1, 2, 3])))  # Numpy and tensor share the same underlying memory.
print(torch.tensor([1, 2, 3]).numpy())

# Kind of equal.
print(float(ones[0]))  # Extract and convert.
print(ones[0].item())  # Extract and keep type.

# Reference.
print(zeros[1])  # tensor([0., 0., 0.])
print(zeros[1][1])  # tensor(0.)

# Slice.
print(zeros[1, :])  # tensor([0., 0., 0.])
print(zeros[1:, 1:])  # tensor([0., 0.])
print(zeros[1, 1])  # tensor(0.)
print(zeros[None])  # tensor([[[0., 0., 0.], [0., 0., 0.]]])

# Unsqueeze.
print(zeros.unsqueeze(dim=0).shape)  # torch.Size([1, 2, 3])
print(zeros.unsqueeze(dim=1).shape)  # torch.Size([2, 1, 3])
print(zeros.unsqueeze(dim=2).shape)  # torch.Size([2, 3, 1])

# Sum and mean.
tensor2x2 = torch.tensor([[1, 2], [3, 4]], dtype=float)
print(tensor2x2.sum(0))  # tensor([4., 6.]) = [1+3, 2+4]
print(tensor2x2.sum(1))  # tensor([3., 7.])
print(tensor2x2.mean(0))  # tensor([2., 3.])

# Named.
tensor2x2 = torch.tensor([[1, 2], [3, 4]], dtype=float, names=['x', 'y'])
print(tensor2x2.names)  # ('x', 'y')
weights = torch.tensor([0.5, 1.0], names=['x'])
print(tensor2x2 * weights.align_as(tensor2x2))  # tensor([[0.5, 1], [3, 4]])

weights.rename_('y')
print(tensor2x2 * weights.align_as(tensor2x2))  # tensor([[0.5, 2], [1.5, 4]])

# API.
print(torch.transpose(tensor2x2, 0, 1))  # tensor([[1, 3], [2, 4]])
print(tensor2x2.transpose(0, 1))  # tensor([[1, 3], [2, 4]])
print(tensor2x2.t())  # tensor([[1, 3], [2, 4]]), only works for 2D tensor.

# GPU.
if torch.cuda.is_available():
    print(tensor2x2.to(device='cuda'))
    print(tensor2x2.cuda())
    cuda_tensor = torch.tensor([[4.0, 1.0], [5.0, 3.0], [2.0, 1.0]], device='cuda')
    print(cuda_tensor.cpu())

# Save and load.
torch.save(ones, 'ones.t')
print(torch.load('ones.t'))
# Or:
with open('ones.t', 'wb') as fout:
    torch.save(ones, fout)
with open('ones.t', 'rb') as fin:
    print(torch.load(fin))

# Save and load as HDF5.
with h5py.File('ones.hdf5', 'w') as fout:
    fout.create_dataset('tensor', data=ones.numpy())
with h5py.File('ones.hdf5', 'r') as fin:
    print(fin['tensor'])
