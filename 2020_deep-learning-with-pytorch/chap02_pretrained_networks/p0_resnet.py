#!/usr/bin/env python

import os

import torch
from PIL import Image
from torchvision import models
from torchvision import transforms

from chap02_pretrained_networks.imagenet_classes import ImagenetClasses


# Prepare input.
preprocess = transforms.Compose([
    transforms.Resize(256),
    transforms.CenterCrop(224),
    transforms.ToTensor(),
    transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),
])

img_path = os.path.join(os.path.dirname(__file__), 'dog.jpg')
img = Image.open(img_path)
img_t = preprocess(img)
batch_t = torch.unsqueeze(img_t, 0)

# Load model.
resnet = models.resnet101(pretrained=True)

# Inference.
resnet.eval()
out = resnet(batch_t)

# Top 5 guesses.
probabilities = torch.nn.functional.softmax(out, dim=1)[0]
_, indices = torch.sort(out, descending=True)
for index in indices[0][:5]:
    index = index.item()
    label = ImagenetClasses[index]
    print(F'Input is a "{label}" with probability = {probabilities[index]}')
