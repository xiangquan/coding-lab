#!/usr/bin/env python

import torch


epochs = 30000


def main():
    X = [[float(i)] for i in range(100)]
    y = [[2.3 * x[0] + 3.2] for x in X]
    t_c = torch.tensor(X)
    t_u = torch.tensor(y)
    model = torch.nn.Linear(1, 1)
    criterion = torch.nn.MSELoss()
    optimizer = torch.optim.SGD(model.parameters(), lr=0.0003)
    for epoch in range(epochs):
        outputs = model(t_c)
        loss = criterion(outputs, t_u)

        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        if epoch % (epochs // 100) == 0:
            print(F'[epoch {epoch}] Train loss: {loss.item()}')

    print(model.weight)
    print(model.bias)


if __name__ == '__main__':
    main()
