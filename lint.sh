#!/usr/bin/env bash
#
# Usage:
#   lint.sh [path]

CLANG_FORMAT="clang-format -i"
FLAKE8="flake8 --max-line-length 100"
AUTOPEP8="autopep8 --in-place --max-line-length 100 -a"
BUILDIFIER="buildifier"

set -e

function LintDir() {
  find "$1" -type f \( -name "*.h" -o -name "*.cc" -o -name "*.cpp" \) -exec ${CLANG_FORMAT} '{}' \;
  find "$1" -type f -name '*.py' -exec ${AUTOPEP8} '{}' \;
  find "$1" -type f -name '*.py' -exec ${FLAKE8} '{}' \;
  find "$1" -type f -name 'BUILD' -exec ${BUILDIFIER} '{}' \;
}

PATH_ARG=$1
if [ -z "${PATH_ARG}" ]; then
  cd "$( dirname "${BASH_SOURCE[0]}" )"
  ls -d */ | while read DIR ; do
    LintDir "${DIR}"
  done
elif [ -d "${PATH_ARG}" ]; then
  LintDir "${PATH_ARG}"
fi
