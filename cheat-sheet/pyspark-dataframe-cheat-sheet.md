# PySpark DataFrame Cheat Sheet

Credit to [Spark By Examples](https://sparkbyexamples.com/pyspark-tutorial/)

The first thing you need to know is that DataFrame is immutable. Most of the code snippets bellow
will create new DataFrames, while not change the old one in-place.

## Create DataFrame

```python
# Init SparkSession.
from pyspark.sql import SparkSession
spark = SparkSession.builder.appName('foobar').getOrCreate()

# Create schema. See https://spark.apache.org/docs/latest/sql-ref-datatypes.html for more types.
from pyspark.sql.types import StructType, StructField, StringType, LongType
schema = StructType([
    StructField('name', StringType(), nullable=False),
    StructField('age', LongType(), nullable=True),
])

# From list.
spark.createDataFrame([('foo', 5), ('bar', 4)], ['name', 'age'])
spark.createDataFrame([('foo', 5), ('bar', 4)], schema)

# From RDD.
rdd = spark.sparkContext.parallelize([('foo', 5), ('bar', 4)])
spark.createDataFrame(rdd, ['name', 'age'])
spark.createDataFrame(rdd, schema)
rdd.toDF(['name', 'age'])
rdd.toDF(schema)
# To RDD.
df.rdd

# From Row list.
from pyspark.sql.types import Row
spark.createDataFrame([Row('foo', 5), Row('bar', 4)], ['name', 'age'])
spark.createDataFrame([Row(name='foo', count=5), Row(name='bar', count=4)])
spark.createDataFrame([Row('foo', 5), Row('bar', 4)], schema)

Instance = Row('name', 'age')
spark.createDataFrame([Instance('foo', 5), Instance('bar', 4)])
# To Row list.
df.collect()

# From data sources.
spark.read.csv('/src/resources/file.csv')
spark.read.text('/src/resources/file.txt')
spark.read.json('/src/resources/file.json')
```

Other data sources:
1. [Parquet file](https://sparkbyexamples.com/pyspark/pyspark-read-and-write-parquet-file/)
1. [Avro files](https://sparkbyexamples.com/spark/using-avro-data-files-from-spark-sql-2-4/)
1. [Kafka streaming](https://sparkbyexamples.com/spark/spark-streaming-kafka-consumer-example-in-json-format/)

## Visualize & Debug DataFrame

```python
df = spark.createDataFrame([('foo', 5), ('bar', 4)], ['name', 'age'])

# Show schema.
df.printSchema()
print(df.schema.fieldNames())  # List of string.
print(df.columns)              # List of string.
print(df.schema.fields)        # List of StructField.
print(df.schema.json())
print(df.schema.simpleString())

# Show data.
df.show()                # Show 20 rows, truncate string to 20 chars.
df.show(5)               # Show 5 rows.
df.show(truncate=10)     # Truncate string to 10 chars.
df.show(truncate=False)  # Don't truncate strings.

# Convert to Pandas to debug. Note that PySpark DF is distributed while Pandas DF is at local.
pandas_df = df.toPandas()
```

## Schema

```python
from pyspark.sql.types import StructType, StructField, StringType, LongType
from pyspark.sql.functions import col

# Nested columns.
schema = StructType([
    StructField('name', StructType([
        StructField('first_name', StringType(), False),
        StructField('middle_name', StringType(), True),
        StructField('last_name', StringType(), False),
    ])),
    StructField('age', LongType(), nullable=True),
])
spark.createDataFrame([(('James', None, 'Smith'), 10),], schema)

# Rename one column.
df.withColumnRenamed('age', 'year')
# Rename all columns.
df.toDF(*['name', 'age'])
# Change DataType.
df.withColumn('age', col("age").cast('String'))
```

## Select Columns

```python
df.select('name')
df.select('name', 'age')
df.select(df.name, df.age)

# Select nested fields.
df.select('name.last_name', 'age')    # Get ['last_name', 'age']
df.select('name.*', 'age')            # Get ['first_name', 'middle_name', 'last_name', 'age']
df.select(df.name.last_name, df.age)  # Get ['name.last_name', 'age']

# Select with alias name.
df.select(col('name.last_name').alias('LastName'), col('age'))
```

## Update, Add and Drop Columns

```python
from pyspark.sql.functions import col, lit

# Update a column.
df.withColumn('age', col('age') * 10)

# Add a column.
df.withColumn('salary', col('age') * 10)
# Add a column with constant value.
df.withColumn('salary', lit(100))

# Drop a column.
df.drop('age')
```

## Filter Rows

```python
from pyspark.sql.functions import col, array_contains

# Single condition.
df.filter(df.age < 5)
df.filter(df.name.last_name == 'Smith')
df.filter(col('age') > 5)
df.filter('age > 5')  # SQL style.

# Multiple conditions.
df.filter((df.age > 5) & (df.age < 11))
df.filter((col('age') > 5) & (col('age') < 11))
df.filter('age > 5 and age < 11')

# Filter on Array column.
df.filter(array_contains(df.languages, 'Java'))
```

See [more SQL functions](https://sparkbyexamples.com/spark/spark-sql-functions/).

`where()` is exactly the same with `filter()`, while it looks more SQL-like. Just stick to the one
you like.

## Drop Rows

```python
# Distinct all columns.
df.distinct()
df.dropDuplicates()
# Distinct on selected columns.
df.dropDuplicates(['age'])
```

## Sort Rows

```python
from pyspark.sql.functions import col

# Syntax: df.sort(*cols)
df.sort('age')
df.sort('age', 'name.last_name')
df.sort(col('age'), col('name.last_name'))
df.sort(df.age, df.name.last_name)

# Sort by descending.
df.sort(df.age.asc(), df.name.last_name.desc())
# Handle null values.
df.sort(col('age').asc(),
        col('name.last_name').asc_nulls_first(),
        col('name.first_name').asc_nulls_last())

# Raw SQL.
df.createOrReplaceTempView("haha")
spark.sql('SELECT name, age FROM haha ORDER BY age asc, name desc')
```

`orderBy()` is exactly the same with `sort()`, while it looks more SQL-like. Just stick to the one
you like.

## Group by

```python
# Got GroupedData.
gd = df.groupBy('age')
gd = df.groupBy('age', 'name.last_name')

# Aggregate GroupedData to get another DataFrame.
gd.count()
gd.mean()  # Syntax: gd.mean(*cols)
gd.avg()   # Syntax: gd.avg(*cols)
gd.max()   # Syntax: gd.max(*cols)
gd.min()   # Syntax: gd.min(*cols)
gd.sum()   # Syntax: gd.sum(*cols)

# Multiple aggregations.
import pyspark.sql.functions as F
gd.agg(
    F.sum('age').alias('total_age'),
    F.avg('age').alias('avg_age')
)
```

`gd.pivot()` is not covered here. Please refer to
[Pivot Table and Unpivot a Spark Dataframe](https://sparkbyexamples.com/spark/how-to-pivot-table-and-unpivot-a-spark-dataframe)

## Join

```python
# Inner join (Null disallowed).
df.join(other, df.other_id == other.id, 'inner')

# Outer join (Null allowed for both sides).
df.join(other, df.other_id == other.id, 'outer')  # Or 'full', 'fullouter'

# Left join (Null allowed for right).
df.join(other, df.other_id == other.id, 'left')  # Or 'leftouter'

# Right join (Null allowed for left).
df.join(other, df.other_id == other.id, 'right')  # Or 'rightouter'

# Left-semi join (Return columns from left for matched records).
df.join(other, df.other_id == other.id, 'leftsemi')

# Left-anti join (Return columns from left for non-matched records).
df.join(other, df.other_id == other.id, 'leftanti')

# Self-join.
df.alias('df1').join(df.alias('df2'),
                     col('df1.superior_id') == col('df2.id'),
                     'inner'
).select(col('df1.id'),
         col('df1.name'),
         col('df1.superior_id'),
         col('df2.name').alias('superior_name'))

# SQL expression.
df.createOrReplaceTempView('df1')
df.createOrReplaceTempView('df2')
spark.sql('SELECT * FROM df1, df2 WHERE df1.superior_id == df2.id')

# Merge dataframes.
df.union(other)
```

## Map

```python
from pyspark.sql.functions import explode

# You need to convert to RDD.
df.rdd.map(func).toDF(df.schema)

# Similar to RDD.flatMap(), it will flatten the array or map columns.
df.select(df.name, explode(df.knownLanguages))
```

## Sample and Fill

```python
df.sample(0.1)
df.sample(0.1, seed=0)

# "withReplacement=True" may result in duplicates.
df.sample(True, 0.1)

# Stratified sampling.
df.sampleBy('category', {'car': 0.1, 'bike': 0.2})

# Replace None to value.
df.fillna(100)
df.fillna(100, ['salary'])
df.fillna({'salary': 100})
df.na.fill(100)
df.na.fill(100, ['salary'])
df.na.fill({'salary': 100})
```

## UDF

UDF’s are a black box to PySpark hence it can’t apply optimization and you will lose all the
optimization PySpark does on Dataframe/Dataset. When possible you should use Spark SQL built-in
functions as these functions provide optimization. Consider creating UDF only when existing built-in
SQL function doesn’t have it.

```python
from pyspark.sql.types import LongType
from pyspark.sql.functions import col, udf

# Define UDF.
def func(a):
    return 1
my_udf = udf(func, LongType())

# Or with annotation.
@udf(returnType=LongType()) 
def my_udf(a):
    return 1

# Use UDF.
df.select(col('name'), my_udf(col('age')))
df.withColumn('one', my_udf(col('age')))

# Register UDF. Not needed if it's defined with annotation.
spark.udf.register("my_udf", func, LongType())
# Use in SQL.
df.createOrReplaceTempView('df1')
spark.sql("SELECT name, my_udf(age) AS one FROM df1")
```
