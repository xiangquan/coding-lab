// Compile: rustc 0_hello-world.rs -o main
// Run:     ./main
fn main() {
    println!("Hello, world!");
}
